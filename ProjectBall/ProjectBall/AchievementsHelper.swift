//
//  AchievementsHelper.swift
//  ProjectBall
//
//  Created by Leandro on 16/06/15.
//  Copyright (c) 2015 LPR. All rights reserved.
//

import Foundation
import GameKit

//// Variáveis para achievements
//
///*	Breaker: 10 blocos (Quebrador ) ( 25 pontos)
//	Crusher:	 50 blocos (Esmagador ) ( 50 pontos)
//	Destroyer:	200 blocos (Destruidor) (100 pontos)*/
//var blocksBroken : Int = 0 //quebrar um determinado número de blocos (3 vezes)
//
///*	Walker: 5000 pontos  (Andarilho  ) ( 25 pontos)
//	Runner:		10000 pontos (Corredor   ) ( 50 pontos)
//	Marathoner:	20000 pontos (Maratonista) (100 pontos)*/
//var scoreValue : Int = 0 //chegar a uma determinada pontuação (3 vezes)
//
///*	Mage:	 10 bônus (Mago      ) ( 25 pontos)
//	Wizard:		 25 bônus (Feiticeiro) ( 50 pontos)
//	Arch-Mage:	 50 bônus (Arquimago ) (100 pontos)*/
//var bonusesTaken : Int = 0 //pegar um determinado número de bônus (3 vezes)
//
//var fireBonusesTaken  : Int = 0 //jogue uma partida pegando só bônus de fogo		(pegar 10) ( 50 pontos)
//var ghostBonusesTaken : Int = 0 //jogue uma partida pegando só bônus de fantasma	(pegar 10) ( 50 pontos)
//var gumBonusesTaken   : Int = 0 //jogue uma partida pegando só bônus de chiclete	(pegar 10) (100 pontos)
//var leadBonusesTaken  : Int = 0 //jogue uma partida pegando só bônus de chumbo	(pegar 10) (100 pontos)
//var waterBonusesTaken : Int = 0 //jogue uma partida pegando só bônus de água		(pegar 10) ( 75 pontos)
//
//var idleBall : Bool = false //Morra sem mover a bola (Bola Parada) ( 25 pontos)
//var iDontNeedHelp : Bool = false //Chegue a 7500 pontos sem pegar bônus (Eu não preciso de ajuda) ( 75 pontos)

class AchievementsHelper {
	
	struct Achievements {
		static let BreakerAchievementId		= "com.leandro.ProjectBall.breaker"
		static let CrusherAchievementId		= "com.leandro.ProjectBall.Crusher"
		static let DestroyerAchievementId	= "com.leandro.ProjectBall.Destroyer"
		static let WalkerAchievementId		= "com.leandro.ProjectBall.Walker"
		static let RunnerAchievementId		= "com.leandro.ProjectBall.Runner"
		static let MarathonerAchievementId	= "com.leandro.ProjectBall.Marathoner"
		static let MageAchievementId		= "com.leandro.ProjectBall.Mage"
		static let WizardAchievementId		= "com.leandro.ProjectBall.Wizard"
		static let ArchMageAchievementId	= "com.leandro.ProjectBall.ArchMage"
		static let FireMasterAchievementId	= "com.leandro.ProjectBall.FireMaster"
		static let GhostMasterAchievementId	= "com.leandro.ProjectBall.GhostMaster"
		static let GumMasterAchievementId	= "com.leandro.ProjectBall.GumMaster"
		static let LeadMasterAchievementId	= "com.leandro.ProjectBall.LeadMaster"
		static let WaterMasterAchievementId	= "com.leandro.ProjectBall.WaterMaster"
		static let IdleBallAchievementId	= "com.leandro.ProjectBall.IdleBall"
		static let IDontNeedHelpAchievementId = "com.leandro.ProjectBall.IDontNeedHelp"
	}
	
	class func breakerAchievement(blocksBroken: Int) -> GKAchievement {
		let blocksMax = 10
		let percent = (blocksBroken/blocksMax) * 100
		
		let breakerAchievement = GKAchievement(identifier: Achievements.BreakerAchievementId)
		
		breakerAchievement.percentComplete = Double(percent)
		breakerAchievement.showsCompletionBanner = true
		
		return breakerAchievement
	}
	
	class func crusherAchievement(blocksBroken: Int) -> GKAchievement {
		let blocksMax = 50
		let percent = (blocksBroken/blocksMax) * 100
		
		let crusherAchievement = GKAchievement(identifier: Achievements.CrusherAchievementId)
		
		crusherAchievement.percentComplete = Double(percent)
		crusherAchievement.showsCompletionBanner = true
		
		return crusherAchievement
	}
	
	class func destroyerAchievement(blocksBroken: Int) -> GKAchievement {
		let blocksMax = 200
		let percent = (blocksBroken/blocksMax) * 100
		
		let destroyerAchievement = GKAchievement(identifier: Achievements.DestroyerAchievementId)
		
		destroyerAchievement.percentComplete = Double(percent)
		destroyerAchievement.showsCompletionBanner = true
		
		return destroyerAchievement
	}
	
	class func walkerAchievement(scoreValue: Int) -> GKAchievement {
		let scoreMax = 5000
		let percent = (scoreValue/scoreMax) * 100
		
		let walkerAchievement = GKAchievement(identifier: Achievements.WalkerAchievementId)
		
		walkerAchievement.percentComplete = Double(percent)
		walkerAchievement.showsCompletionBanner = true
		
		return walkerAchievement
	}
	
	class func runnerAchievement(scoreValue: Int) -> GKAchievement {
		let scoreMax = 10000
		let percent = (scoreValue/scoreMax) * 100
		
		let runnerAchievement = GKAchievement(identifier: Achievements.RunnerAchievementId)
		
		runnerAchievement.percentComplete = Double(percent)
		runnerAchievement.showsCompletionBanner = true
		
		return runnerAchievement
	}
	
	class func marathonerAchievement(scoreValue: Int) -> GKAchievement {
		let scoreMax = 20000
		let percent = (scoreValue/scoreMax) * 100
		
		let marathonerAchievement = GKAchievement(identifier: Achievements.MarathonerAchievementId)
		
		marathonerAchievement.percentComplete = Double(percent)
		marathonerAchievement.showsCompletionBanner = true
		
		return marathonerAchievement
	}
	
	class func mageAchievement(bonusesTaken: Int) -> GKAchievement {
		let bonusesMax = 10
		let percent = (bonusesTaken/bonusesMax) * 100
		
		let mageAchievement = GKAchievement(identifier: Achievements.MageAchievementId)
		
		mageAchievement.percentComplete = Double(percent)
		mageAchievement.showsCompletionBanner = true
		
		return mageAchievement
	}
	
	class func wizardAchievement(bonusesTaken: Int) -> GKAchievement {
		let bonusesMax = 25
		let percent = (bonusesTaken/bonusesMax) * 100
		
		let wizardAchievement = GKAchievement(identifier: Achievements.WizardAchievementId)
		
		wizardAchievement.percentComplete = Double(percent)
		wizardAchievement.showsCompletionBanner = true
		
		return wizardAchievement
	}
	
	class func archMageAchievement(bonusesTaken: Int) -> GKAchievement {
		let bonusesMax = 50
		let percent = (bonusesTaken/bonusesMax) * 100
		
		let archMageAchievement = GKAchievement(identifier: Achievements.ArchMageAchievementId)
		
		archMageAchievement.percentComplete = Double(percent)
		archMageAchievement.showsCompletionBanner = true
		
		return archMageAchievement
	}
	
	class func fireMasterAchievement(fireBonusesTaken: Int) -> GKAchievement {
		let bonusesMax = 10
		let percent = (fireBonusesTaken/bonusesMax) * 100
		
		let fireMasterAchievement = GKAchievement(identifier: Achievements.FireMasterAchievementId)
		
		fireMasterAchievement.percentComplete = Double(percent)
		fireMasterAchievement.showsCompletionBanner = true
		
		return fireMasterAchievement
	}
	
	class func ghostMasterAchievement(ghostBonusesTaken: Int) -> GKAchievement {
		let bonusesMax = 10
		let percent = (ghostBonusesTaken/bonusesMax) * 100
		
		let ghostMasterAchievement = GKAchievement(identifier: Achievements.GhostMasterAchievementId)
		
		ghostMasterAchievement.percentComplete = Double(percent)
		ghostMasterAchievement.showsCompletionBanner = true
		
		return ghostMasterAchievement
	}
	
	class func gumMasterAchievement(gumBonusesTaken: Int) -> GKAchievement {
		let bonusesMax = 10
		let percent = (gumBonusesTaken/bonusesMax) * 100
		
		let gumMasterAchievement = GKAchievement(identifier: Achievements.GumMasterAchievementId)
		
		gumMasterAchievement.percentComplete = Double(percent)
		gumMasterAchievement.showsCompletionBanner = true
		
		return gumMasterAchievement
	}
	
	class func leadMasterAchievement(leadBonusesTaken: Int) -> GKAchievement {
		let bonusesMax = 10
		let percent = (leadBonusesTaken/bonusesMax) * 100
		
		let leadMasterAchievement = GKAchievement(identifier: Achievements.LeadMasterAchievementId)
		
		leadMasterAchievement.percentComplete = Double(percent)
		leadMasterAchievement.showsCompletionBanner = true
		
		return leadMasterAchievement
	}
	
	class func waterMasterAchievement(waterBonusesTaken: Int) -> GKAchievement {
		let bonusesMax = 10
		let percent = (waterBonusesTaken/bonusesMax) * 100
		
		let waterMasterAchievement = GKAchievement(identifier: Achievements.WaterMasterAchievementId)
		
		waterMasterAchievement.percentComplete = Double(percent)
		waterMasterAchievement.showsCompletionBanner = true
		
		return waterMasterAchievement
	}
	
	class func idleBallAchievement() -> GKAchievement {
		
		let idleBallAchievement = GKAchievement(identifier: Achievements.IdleBallAchievementId)
		
		idleBallAchievement.percentComplete = 100
		idleBallAchievement.showsCompletionBanner = true
		
		return idleBallAchievement
	}
	
	class func iDontNeedHelpAchievement() -> GKAchievement {
		
		let iDontNeedHelpAchievement = GKAchievement(identifier: Achievements.IDontNeedHelpAchievementId)
		
		iDontNeedHelpAchievement.percentComplete = 100
		iDontNeedHelpAchievement.showsCompletionBanner = true
		
		return iDontNeedHelpAchievement
	}
}
