//
//  GameScene.swift
//  ProjectBall
//
//  Created by Leandro on 12/03/15.
//  Copyright (c) 2015 LPR. All rights reserved.
//

import SpriteKit
import AVFoundation
import AudioToolbox
import GameKit
import iAd

let fontName : String = "Thonburi-Bold"
var background = "backgroundGame"
let backgrounds = [
	"backgroundGame": SKTexture(imageNamed:"backgroundGame"),
	"backgroundGame2" : SKTexture(imageNamed:"backgroundGame2" ),
	"backgroundGame3" : SKTexture(imageNamed:"backgroundGame3" ),
	"backgroundGame4" : SKTexture(imageNamed:"backgroundGame4" )]

class GameScene: GameSceneInterface, SKPhysicsContactDelegate, ADInterstitialAdDelegate {
	// Variáveis para achievements
	
	/*	Breaker: 10 blocos (Quebrador ) ( 25 pontos)
	Crusher:	 50 blocos (Esmagador ) ( 50 pontos)
	Destroyer:	200 blocos (Destruidor) (100 pontos)*/
	var blocksBroken : Int = 0 //quebrar um determinado número de blocos
	
	/*	Walker:	 5000 pontos (Andarilho  ) ( 25 pontos)
	Runner:		10000 pontos (Corredor   ) ( 50 pontos)
	Marathoner:	20000 pontos (Maratonista) (100 pontos)*/
	var scoreValue : Int = 0 //chegar a uma determinada pontuação
	
	/*	Mage:	 10 bônus (Mago      ) ( 25 pontos)
	Wizard:		 25 bônus (Feiticeiro) ( 50 pontos)
	Arch-Mage:	 50 bônus (Arquimago ) (100 pontos)*/
	var bonusesTaken : Int = 0 //pegar um determinado número de bônus
	
	var  fireBonusesTaken : Int = 0 //jogue uma partida pegando só bônus de fogo	 (pegar 10) ( 50 pontos)
	var ghostBonusesTaken : Int = 0 //jogue uma partida pegando só bônus de fantasma (pegar 10) ( 50 pontos)
	var   gumBonusesTaken : Int = 0 //jogue uma partida pegando só bônus de chiclete (pegar 10) (100 pontos)
	var  leadBonusesTaken : Int = 0 //jogue uma partida pegando só bônus de chumbo	 (pegar 10) (100 pontos)
	var waterBonusesTaken : Int = 0 //jogue uma partida pegando só bônus de água	 (pegar 10) ( 75 pontos)
	
	var ballMoved : Bool = false //Variável auxiliar para idleBall
	var idleBall : Bool = false //Morra sem mover a bola (Bola Parada) ( 25 pontos)
	var iDontNeedHelp : Bool = false //Chegue a 7500 pontos sem pegar bônus ( 75 pontos)
	
	//iAd
	var iAdOn : Bool!
	var interstitial: ADInterstitialAd?
	var interstitialAdView: UIView = UIView()
	var verificationOfSoundAd = false // auxilia na desativação do som durante a exibição do iAd
	var verificationScreenAd = false

	//User Defaults
	let userDefaults = NSUserDefaults.standardUserDefaults()
	
	/* maior pontuação */
	let highScoreKey = "HighScore"
	var highScore = userDefaultManager.retrieveElementForKey("highScore") as? Int
	
	var ballType : String!
	var ballSprite : Ball!
	
	var backgroundMovePointsPerSec : CGFloat = 200.0
	var lastUpdateTime : NSTimeInterval = 0
	var dt : NSTimeInterval = 0
	
	var timeToUntransform : CGFloat = 0.0
	var velocityBeforeContact : CGVector = CGVector(dx: 0, dy: 0)
	var bonusDuration : CGFloat = 0.0
	
	//Bola está concentrando
	var ballIsConcentrating : Bool = false
	
	//Nova borda podendo matar a bola
	let playableRect : CGRect = CGRect(x: 192, y: -150, width: 1152, height: 2198)
	
	var pointsToSpawnObstacles : CGFloat = 0.0
	let distanceToSpawnObstacles : CGFloat = 1000
	
	// Último ponto clicado
	var lastTouchedPosition : CGPoint = CGPointZero
	
	/* label pontuação */
	let score = SKLabelNode(fontNamed: fontName)
	let bonusTime = SKLabelNode(fontNamed: fontName)/* label tempo efeito bonus */
	/* ponto clicado */
	var shapeNode : SKShapeNode = SKShapeNode()
	/* nós de background */
	let backgroundNode = SKSpriteNode()
	let background0 = SKSpriteNode()
	let background1 = SKSpriteNode()
	/* código pausa do jogo */
	let buttonPauseBoundaries = SKSpriteNode(color: UIColor.blueColor(), size: CGSize(width: 150, height: 150))
	let buttonPause = SKSpriteNode(imageNamed: "pause")
	let screenMenuPause = SKSpriteNode(imageNamed: "backgroundPause") // tela pause
	let buttonHomeMenuPause = SKSpriteNode(imageNamed: "home") // buttonHome
	let labelMenuPause = SKLabelNode(fontNamed: fontName) // label
	var buttonSound = SKSpriteNode(imageNamed: "soundOnFilled")
	
	/* código fim de jogo */
	let screenMenuGameOver = SKSpriteNode(imageNamed: "backgroundPause")
	let labelMenuGameOver = SKLabelNode(fontNamed: fontName) // label
	let labelScoreMenuGameOver = SKLabelNode(fontNamed: fontName) // label
	let labelHighScoreMenuGameOver = SKLabelNode(fontNamed: fontName) // label
	let buttonHomeMenuGameOver = SKSpriteNode(imageNamed: "home") // buttonHome
	let buttonRestartMenuGameOver = SKSpriteNode(imageNamed: "restart") // buttonHome
	
	var buttonHomeMenuGameOverActive = false
	var buttonRestartMenuGameOverActive = false
	var gameOverActive = false
	
	var buttonHomeMenuPauseActive = false
	var buttonSoundMenuPauseActive = false
	var screenMenuPauseActive = false
	var pauseOfGameActive = false
	
	/* nós de transição das fases */
	let wall = SKSpriteNode(imageNamed: "wall.png")
	let lbStage = SKLabelNode(fontNamed: fontName)
	let colorTransition = SKSpriteNode()
	
	/* actions pros botões */
	let actionTap1 = SKAction.scaleTo(1.15, duration: 0.1)
	let actionTap2 = SKAction.scaleTo(0.85, duration: 0.1)
	let actionTap3 = SKAction.scaleTo(1.00, duration: 0.1)
	
	let actionBall1 = SKAction.scaleTo(0.5, duration: 0.3)
	let actionBall2 = SKAction.scaleTo(1.25, duration: 0.3)
	let actionBall3 = SKAction.scaleTo(1.00, duration: 0.3)
	
	var isSoundOn : Bool = true
	var vibrationOn : Bool = true
	var audioPlayer : AVAudioPlayer!
	var soundConcentrating : AVAudioPlayer!
	
	//Dicionário com texturas de obstáculos bonus para comparação
	let obstaclesBonus = getObstaclesBonus()
	//Variáveis para medir o tempo que segurou o LongPress
	var startedLongPress : NSDate = NSDate()
	var endedLongPress : NSDate = NSDate()
	var ballCharged : Bool = false
	var ballChargedType = "ballDefault"
	//Variável se já perdeu a velocidade
	var ballInsideObstacle : Bool = false
	//Intensidade de queimadura
	var burnPower : Int = 0
	//Fase em que o jogador está
	var stage = 1
	var maxVelocityOfStage : CGFloat = 400.0
	//Variável de tutorial
	var isTutorialOn = true
	//Carregamento de texturas auxiliares
	let ballConcentratingTexture = SKTexture(imageNamed: "ballConcentrating")
	//Categorias de colisão para física
	struct PhysicsCategory {
		static let None: UInt32 = 0
		static let Ball: UInt32 = 0b1 // 1
		static let Obstacle: UInt32 = 0b10 // 2
		static let Edge: UInt32 = 0b1000 // 4
		static let ObstacleBonus: UInt32 = 0b1000 // 8
	}
	init(size: CGSize, sound: Bool, vibration: Bool, iAd: Bool) {
		isSoundOn = sound
		vibrationOn = vibration
		iAdOn = iAd
		super.init(size: size)
	}
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	override func didMoveToView(view: SKView) {
		
		ballType = "ballDefault"
		ballSprite = Ball(imageName: ballType)
		ballSprite.typeOfBall(ballType)
		ballSprite.trail?.targetNode = self
		ballSprite.position = CGPoint(x: size.width/2, y: size.height/2 - 400)
		addChild(ballSprite)
		//Tirando gravidade
		physicsWorld.gravity = CGVector(dx: 0, dy: 0)
		//Aplicando as bordas no mundo
		physicsBody = SKPhysicsBody(edgeLoopFromRect: playableRect)
		
		//Delegando o tratamento de contatos e setando o máscara de contato da borda
		physicsWorld.contactDelegate = self
		physicsBody!.contactTestBitMask = PhysicsCategory.Edge
	
		/* pontuação */
		score.text = "0"
		score.color = UIColor.blackColor()
		score.colorBlendFactor = 1
		score.fontSize = 80
		score.position = CGPoint(x: 1320, y: 1915)
		score.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Right
		score.zPosition = 50

		/* tempo da bola com bonus */
		bonusTime.text = "0"
		bonusTime.color = UIColor.blackColor()
		bonusTime.colorBlendFactor = 1
		bonusTime.fontSize = 75
		bonusTime.position = CGPoint(x: size.width-342, y: 120)
		bonusTime.zPosition = 50
		bonusTime.hidden = true
		
		/* configurando buttonPause */
		buttonPause.size = CGSize(width: 100, height: 100)
		buttonPause.position = CGPoint(x: 292, y: 1948)
		buttonPause.zPosition = 50
		
		/* configurando os limites do buttonPause */
		buttonPauseBoundaries.position = CGPoint(x: 292, y: 1948)
		buttonPauseBoundaries.zPosition = 50
		buttonPauseBoundaries.hidden = true
		
		/* add pause, score e tempo bonus */
		addChild(buttonPause)
		addChild(buttonPauseBoundaries)
		addChild(score)
		addChild(bonusTime)
		
		/******* Sounds *******/
		let soundFilePath = NSBundle.mainBundle().pathForResource("backgroundSound", ofType: "mp3")
		let soundFileURL = NSURL.fileURLWithPath(soundFilePath!)
		do {
			try audioPlayer = AVAudioPlayer(contentsOfURL: soundFileURL)
		} catch {
			print("Sound not reached.")
		}
		
		audioPlayer.numberOfLoops = -1
		audioPlayer.prepareToPlay()
		audioPlayer.volume = 0.0
		audioPlayer.play()
		if isSoundOn {
			doVolumeFadeIn()
		}
		let url = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("effectConcentrating", ofType: "mp3")!)
		
		do {
			try soundConcentrating = AVAudioPlayer(contentsOfURL: url)
		} catch {
			print("Sound not reached.")
		}
		
		//Gesture recognizer LongPress
		let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: "longPressed:")
		longPressRecognizer.minimumPressDuration = 0.2
		self.view?.addGestureRecognizer(longPressRecognizer)
		
		//iAds
		if iAdOn! {
			//self.view?.window?.rootViewController?.canDisplayBannerAds = false
		}
		
		//Configuração de nós de transição
		wall.anchorPoint = CGPointZero
		wall.name = "wall"
		wall.zPosition = 20
		
		lbStage.fontColor = SKColor.whiteColor()
		lbStage.fontSize = 160
		lbStage.position = CGPoint(x: wall.size.width/2, y: (wall.size.height/2+90) - 850)
		
		colorTransition.anchorPoint = CGPointZero
		colorTransition.size = CGSize(width: 1536, height: 1000)
		colorTransition.position = CGPoint(x: 0, y: size.height)
		colorTransition.name = "colorTransition"
		colorTransition.zPosition = 0
		
		//Retorna pro primeiro background
		background = "backgroundGame"
		//Background
		configBackbround()
		updateBackground()
		//Verifica se precisa de tutorial
		/* User Defaults */
		if let tutorialState: AnyObject = userDefaults.valueForKey("isTutorialOn") {
			isTutorialOn = tutorialState.boolValue
		}
		if isTutorialOn{
			startTutorials(self)
		}
	}
	
	/* Fade-in/Fade-out da música de fundo */
	func doVolumeFadeOut() {
		if (audioPlayer.volume >= 0.1) {
			audioPlayer.volume = audioPlayer.volume - 0.1
			//delay de meio segundo para o volume chegar no mínimo
			let delay = 0.05 * Double(NSEC_PER_SEC)
			let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
			dispatch_after(time, dispatch_get_main_queue()) {
				self.doVolumeFadeOut()
			}
		}
	}
	func doVolumeFadeIn() {
		if (audioPlayer.volume < 0.5) {
			audioPlayer.volume = audioPlayer.volume + 0.1
			//delay de meio segundo para o volume chegar no máximo
			let delay = 0.05 * Double(NSEC_PER_SEC)
			let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
			dispatch_after(time, dispatch_get_main_queue()) {
				self.doVolumeFadeIn()
			}
		}
	}
	
	override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
		let sequenceTap = SKAction.sequence([actionTap1, actionTap2, actionTap3])
		
		for touch: AnyObject in touches {
			
			let location = touch.locationInNode(self)
			
			//Mostra toque na tela
			showTapAtLocation(location)
			
			
			/* verifica se o buttonHome foi tocado dentro do menu game over*/
			if (buttonHomeMenuGameOver.containsPoint(location) && buttonHomeMenuGameOverActive == true) {
				actionButtonHome()
			}
				/* verifica se o buttonRestart foi tocado dentro do menu game over */
			else if (buttonRestartMenuGameOver.containsPoint(location) && buttonRestartMenuGameOverActive == true) {
				buttonRestartMenuGameOver.runAction(sequenceTap, completion: {
					self.actionButtonRestart()
				})
			}
				/* verifica se o buttonHome foi tocado dentro do menu pause */
			else if(buttonHomeMenuPause.containsPoint(location) && buttonHomeMenuPauseActive == true) {
				actionButtonHome()
			}
				/* verifica se o buttonSound foi tocado dentro do menu pause */
			else if buttonSound.containsPoint(location) && buttonSoundMenuPauseActive == true {
				if isSoundOn {
					buttonSound.texture = SKTexture(imageNamed: "soundOffFilled")
					buttonSound.runAction(sequenceTap)
					
					//Código para desativar o som de background
					doVolumeFadeOut()
					isSoundOn = false
				}
				else {
					buttonSound.texture = SKTexture(imageNamed: "soundOnFilled")
					buttonSound.runAction(sequenceTap)
					
					//Código para ativar o som de background
					audioPlayer.volume = 0.0
					doVolumeFadeIn()
					isSoundOn = true
				}
			}
				/* verifica se a tela foi tocada no menu pause */
			else if(screenMenuPause.containsPoint(location) &&  buttonHomeMenuPauseActive == true && screenMenuPauseActive == true) {
				menuPause()
			}
				/* verifica se os limites do buttonPause foi tocado */
			else if (buttonPauseBoundaries.containsPoint(location) && buttonPause.userInteractionEnabled == false) {
				buttonPause.runAction(sequenceTap, completion: {
					self.pauseGame()
				})
			}
				/* verifica se um ponto qualquer da tela foi tocado para a movimentação da bola */
			else {
				ballSprite.physicsBody!.velocity = velocityToMoveBall(location)
				if isTutorialOn{
					tutorialDone(TutorialType.MoveBall, scene: self)
				}
			}
		}
	}
	func longPressed(sender: UILongPressGestureRecognizer){
		//Se o cara segurar o botão por mais 0.25, apenas bola
		if(sender.state == UIGestureRecognizerState.Began && ballType != "ballGum" && ballType != "ballLead"){
			concentratingAura(true)     //Começa a carregar a bola
			startedLongPress = NSDate() //Marca o tempo de inicio
		}
		//Evento quando solta
		else if(sender.state == UIGestureRecognizerState.Ended && ballType != "ballGum" && ballType != "ballLead"){
			concentratingAura(false) 			//Desativa a aura
			endedLongPress = NSDate()           //Marca o tempo final
			var touchLocation = sender.locationInView(sender.view)
			touchLocation = self.convertPointFromView(touchLocation)
			let timeHolding = CGFloat(endedLongPress.timeIntervalSinceDate(startedLongPress))
			//1.4 ...
			if timeHolding >= 1.6 - (backgroundMovePointsPerSec/1000.0) {
				if ballChargedType == "ballDefault"{
					ballCharged = true
					let charge = createChargeAura()
					charge.targetNode = self
					ballSprite.addChild(charge)
					ballSprite.physicsBody?.velocity = velocityToMoveBall(touchLocation) * 3.0 //Corre com 3x a velocidade
					let action1 = SKAction.waitForDuration(0.25) //Tempo que a bola fica com poderes de destruição
					let action2 = SKAction.runBlock({
						self.ballCharged = false
						charge.removeFromParent()
					})
					runAction(SKAction.sequence([action1, action2]))
					if self.isSoundOn {
						self.runAction(SKAction.playSoundFileNamed("effectCharge.mp3", waitForCompletion:false))
					}
				}else if ballChargedType == "ballWater" || ballChargedType == "ballFire" || ballChargedType == "ballGhost" && ballType != "ballGum" && ballType != "ballLead"{
					transformationAuraActions(ballChargedType)
					if ballChargedType == "ballWater"{
						//Toca efeito de água
						if isSoundOn {
							runAction(SKAction.playSoundFileNamed("effectWater.mp3", waitForCompletion:false))
						}
						let wait = SKAction.waitForDuration(0.1)
						let move = SKAction.runBlock({ () -> Void in
							self.ballSprite.physicsBody!.velocity = self.velocityToMoveBall(touchLocation)
						})
						runAction(SKAction.sequence([wait, move]))
					}
					//Recoil da bola de fogo
					if ballChargedType == "ballFire"{
						//Toca efeito de explosão
						if isSoundOn {
							runAction(SKAction.playSoundFileNamed("effectFireExplosion.mp3", waitForCompletion:false))
						}
						ballSprite.velocity = 0
						let smoke = createSmoke()
						ballSprite.addChild(smoke)
						let duration = Double(1.2 - (backgroundMovePointsPerSec/1000))
						smoke.runAction(SKAction.fadeOutWithDuration(duration))
						ballSprite.runAction(SKAction.waitForDuration(duration), completion: {
							self.ballSprite.velocityForBall(self.ballType)
							smoke.removeFromParent()
						})
					}
				}
				//Remove as shurikens grudadas na bola
				enumerateChildNodesWithName("ballSprite/obstacle5") { node, _ in
					node.removeFromParent()
				}
				if isTutorialOn{
					tutorialDone(TutorialType.LongPress, scene: self)
				}
			}else{
				ballSprite.physicsBody!.velocity = velocityToMoveBall(touchLocation)
				if isTutorialOn{
					//Repete o tutorial de LongPress
					tutorialDone(TutorialType.MoveBall, scene: self)
				}
			}
		}
	}
	
	func velocityToMoveBall(location: CGPoint) -> CGVector {
		//Atualizando algumas variáveis para mover a bola adequadamente
		ballSprite.physicsBody!.linearDamping = ballSprite.properties[2]
		lastTouchedPosition = location
		ballMoved = true
		
		let velocityPerSec : CGFloat = ballSprite.velocity!
		let offset = CGPoint(x: location.x - ballSprite.position.x, y: location.y - ballSprite.position.y)
		let length = sqrt(Double(offset.x * offset.x + offset.y * offset.y))
		let direction = CGPoint(x: offset.x / CGFloat(length), y: offset.y / CGFloat(length))
		let velocity = CGVector(dx: direction.x * velocityPerSec, dy: direction.y * velocityPerSec)
		if ballSprite.physicsBody?.allContactedBodies().count != 0 && ballType == "ballGhost"{
			return velocity * 0.25
		}
		return velocity
	}
	
	override func didSimulatePhysics() {
		
		//Freia a bola de acordo com a ultima posição tocada.
		if (ballSprite.containsPoint(lastTouchedPosition)) {
			ballSprite.physicsBody!.velocity.dx = ballSprite.physicsBody!.velocity.dx * 0.6
			ballSprite.physicsBody!.velocity.dy = ballSprite.physicsBody!.velocity.dy * 0.6
		}
	}
	
	func didBeginContact(contact: SKPhysicsContact) {
		
		//A bola tocou uma caixa
		if contact.bodyA.categoryBitMask == PhysicsCategory.Obstacle && contact.bodyB.categoryBitMask == PhysicsCategory.Ball && !gameOverActive{
			
			//Pega o obstáculo em contato
			let obstacle = contact.bodyA.node as! SKSpriteNode
			if obstacle.name == "obstacle4" && ballType != "ballFire"{
				//Se a bola for tipo água, apaga o fogo
				if  ballType == "ballWater"{
					obstacle.removeAllChildren()
					let smoke = createSmokeObstacle()
					obstacle.addChild(smoke)
					smoke.runAction(SKAction.waitForDuration(0.7), completion: { () -> Void in
						smoke.removeFromParent()
					})
					obstacle.name = "obstacle3"
					scoreObstacle(bonusForObstacle(obstacle)/2, position: obstacle.position, scene: self)
					if isSoundOn {
						runAction(SKAction.playSoundFileNamed("effectPsst.mp3", waitForCompletion:false))
					}
				}
					//Se não for tipo água, a bola começa a se queimar e pode morrer
				else{
					if isSoundOn {
						runAction(SKAction.playSoundFileNamed("effectBurning.mp3", waitForCompletion:false))
					}
					if let _ = ballSprite.childNodeWithName("particleSmoke") as? SKEmitterNode{
						//Quanto mais tempo toca o obstáculo em fogo, maior a intensidade da queimadura
						burnPower += 10
						//Se a intensidade for maior ou igual a 40, a bola explode
						if burnPower >= 40{
							death()
						}
					}else{
						let smoke = createSmoke()
						ballSprite.addChild(smoke)
						smoke.targetNode = self
						smoke.runAction(SKAction.waitForDuration(1.5), completion: { () -> Void in
							smoke.removeFromParent()
							self.burnPower = 0
						})
					}
				}
			}
			else if obstacle.name == "obstacle5" && ballType != "ballGhost" && ballType != "ballFire" && ballType != "ballLead" && !ballCharged{
				self.scene?.physicsWorld.addJoint(attachObstacleToBall(obstacle: obstacle, ball: self.ballSprite, contactPoint: contact.contactPoint))
				let deattach = SKAction.runBlock({ () -> Void in
					obstacle.removeFromParent()
				})
				obstacleFlashing(obstacle)
				runAction(SKAction.sequence([SKAction.waitForDuration(3.0), deattach]))
				if ballType == "ballGum"{
					obstacle.physicsBody?.density = 0.1
				}else{
					let dx = ballSprite.position.x - obstacle.position.x
					var dy = ballSprite.position.y - obstacle.position.y
					//Força que joga a bola pra trás
					if dy < 0{
						dy = dy * 10
					}
					ballSprite.physicsBody?.applyImpulse(CGVector(dx: dx, dy: dy))
					//Peso da shuriken na bola default
					obstacle.physicsBody?.density = 8.0
				}
			}
			else if (ballType == "ballGhost" && !ballInsideObstacle){
				ballSprite.physicsBody?.velocity = ballSprite.physicsBody!.velocity * 0.25
				ballInsideObstacle = true
			}
			else if (ballCharged) {
				//Se a velocidade for maior que 500, remove o obstaculo e perde velocidade
				if (velocityBeforeContact > 500) {
					//explodir obstáculo
					addChild(createExplosionBox(obstacle))
					if isSoundOn{
						runAction(SKAction.playSoundFileNamed("effectFire.mp3", waitForCompletion:false))
					}
					//Achievement
					blocksBroken++
					scoreObstacle(bonusForObstacle(obstacle), position: obstacle.position, scene: self)
					obstacle.removeFromParent()
					ballSprite.physicsBody!.velocity = velocityBeforeContact * 0.75
				}
			}
			else if ballType == "ballFire" && obstacle.name != "obstacle4"{
				let fire = createBurningBox(obstacle)
				addChild(fire)
				//addChild(vanishObstacle(obstacle))
				let up = SKAction.moveByX(0, y: obstacle.frame.height * 0.8, duration: 0.5)
				let disappear = SKAction.removeFromParent()
				fire.runAction(SKAction.sequence([up, disappear])){
					obstacle.removeFromParent()
				}
				if isSoundOn{
					runAction(SKAction.playSoundFileNamed("effectFire.mp3", waitForCompletion:false))
				}
				//Achievement
				blocksBroken++
				scoreObstacle(bonusForObstacle(obstacle), position: obstacle.position, scene: self)
				ballSprite.physicsBody?.velocity = velocityBeforeContact * 0.75
			}
			else if ballType == "ballLead" && (obstacle.name == "obstacle2" || obstacle.name == "obstacle3" || obstacle.name == "obstacle5"){
				if isSoundOn {
					runAction(SKAction.playSoundFileNamed("effectCollision.mp3", waitForCompletion:false))
				}
				//Se a bola de chumbo bater na shuriken ou retângulo
				pushObstacle(obstacle, contact: contact)
				scoreObstacle(bonusForObstacle(obstacle)/2, position: obstacle.position, scene: self)
				ballSprite.physicsBody?.velocity = velocityBeforeContact * 0.75
			}
			else{
				if isSoundOn {
					runAction(SKAction.playSoundFileNamed("effectCollision.mp3", waitForCompletion:false))
				}
			}
		}
		
		//A bola tocou um Bonus
		if contact.bodyB.categoryBitMask == PhysicsCategory.ObstacleBonus && contact.bodyA.categoryBitMask == PhysicsCategory.Ball && !gameOverActive{
			//Destransforma caso esteja transormada
			if ballType != "ballDefault"{
				untransformBall()
			}
			//Pausa o tempo de tranformação
			timeToUntransform = 0
			bonusTime.hidden = true
			//Toca o som da transformação
			if isSoundOn{
				runAction(SKAction.playSoundFileNamed("effectTransformation.wav", waitForCompletion:false))
			}
			//Anima a transformação
			ballSprite.runAction(SKAction.sequence([actionBall1, actionBall2, actionBall3]))
			
			enumerateChildNodesWithName("chosenBonus") { node, _ in
				let bonusNode = node as! SKSpriteNode
				bonusNode.removeFromParent()
			}
			
			//Pega o obstáculo em contato
			let bonus = contact.bodyB.node as! SKSpriteNode
			let bonusPosition = bonus.position
			let bonusTexture = bonus.texture
			
			bonus.removeFromParent()
			
			//Transforma o obstáculo em um contador
			let powerUp = SKSpriteNode(texture: bonusTexture, color:UIColor.clearColor(), size:CGSize(width: size.width*0.08, height: size.width*0.08))
			powerUp.position = bonusPosition
			powerUp.name = "chosenBonus"
			powerUp.zPosition = 2
			
			addChild(powerUp)
			
			//Achievement
			bonusesTaken++
			if bonusTexture == obstaclesBonus["water"] {
				bonusDuration = 12.0
				//Achievement
				waterBonusesTaken++
				ballType = "ballWater"
			} else if bonusTexture == obstaclesBonus["fire"] {
				bonusDuration = 6.0
				//Achievement
				fireBonusesTaken++
				ballType = "ballFire"
			} else if bonusTexture == obstaclesBonus["ghost"] {
				bonusDuration = 6.0
				//Achievement
				ghostBonusesTaken++
				ballType = "ballGhost"
			} else if bonusTexture == obstaclesBonus["gum"] {
				bonusDuration = 12.0
				//Achievement
				gumBonusesTaken++
				ballType = "ballGum"
			} else if bonusTexture == obstaclesBonus["lead"] {
				bonusDuration = 8.0
				//Achievement
				leadBonusesTaken++
				ballType = "ballLead"
			} else {
				ballType = "ballDefault"
			}
			
			let animation1 = SKAction.sequence([actionTap1, actionTap2, actionTap3])
			let animation2 = SKAction.moveTo(CGPoint(x: size.width-342, y: 150), duration: 1.0)
			let animation3 = SKAction.scaleTo(1.4, duration: 0.1)
			let sequenceBonusTime = SKAction.group([animation1, animation2, animation3])
			
			self.ballSprite.typeOfBall(self.ballType)
			self.ballSprite.trail?.targetNode = self
			
			powerUp.runAction(sequenceBonusTime, completion: {
				self.timeToUntransform = self.bonusDuration
			})
			
		}
	}
	
	
	func didEndContact(contact: SKPhysicsContact) {
		if contact.bodyA.categoryBitMask == PhysicsCategory.Obstacle && contact.bodyB.categoryBitMask == PhysicsCategory.Ball {
			if ballType == "ballGhost" && ballInsideObstacle{
				ballSprite.physicsBody?.velocity = ballSprite.physicsBody!.velocity * 4.0
				ballInsideObstacle = false
			}
		}
	}
	
	override func update(currentTime: CFTimeInterval) {
		if lastUpdateTime > 0 {
			dt = currentTime - lastUpdateTime
		}
		else {
			dt = 0
		}
		lastUpdateTime = currentTime
		
		if (gameOverActive != true) {
			
			if (pauseOfGameActive != true) {
				
				if(ballSprite.position.y <= -74) {
					death()
				}
				else {
					//Continua tela
					
					//Achievement
					if (scoreValue == 7500) && (bonusesTaken == 0) {
						iDontNeedHelp = true
					} else {
						iDontNeedHelp = false
					}
					
					//AchievementReport
					reportAchievements()
					
					updateVelocity()
					moveNodes()
					
					pointsToSpawnObstacles -= CGFloat(dt) * backgroundMovePointsPerSec
					
					if pointsToSpawnObstacles < 0.0 {
						pointsToSpawnObstacles = distanceToSpawnObstacles
						tryToSpawnObstacles()
					}
					
					if timeToUntransform > 0.0 {
						timeToUntransform -= CGFloat(dt)
						bonusTime.hidden = false
						bonusTimeDuration()
						
						if timeToUntransform <= 0.0 {
							untransformBall()
							bonusTime.hidden = true
						}
					}
					
					if ballSprite.physicsBody?.velocity != CGVector(dx: 0, dy: 0){
						velocityBeforeContact =  ballSprite.physicsBody!.velocity
					}
				}
			}
		}
	}
	
	func tryToSpawnObstacles(){
		if backgroundMovePointsPerSec < maxVelocityOfStage || stage >= 4{
			spawnObstacles()
			//Teste pra remover todos os nós fora da tela
//			enumerateChildNodesWithName("//*") { node, _ in
//				if node.position.y - node.frame.height < 0{
//					node.removeFromParent()
//				}
//			}
		}else{
				stage++
				startStage(stage, scene: self)
		}
	}
	
	func spawnObstacles() {
		//Agora iremos spawnar 2 ondas de obstáculos por vez para melhor aplicação do bonus
		//Primeira onda
		var numberOfObstacles1 = arc4random_uniform(3) + 2
		var regions1 = [307.2, 537.6, 768, 998.4, 1228.8]
		var triangle : SKSpriteNode?
		for _ in 1...numberOfObstacles1{
			if regions1.count > 0{
				let obstacle = generateRandomObstacle(&regions1, heightToSpawn: size.height, stage: stage)
				addChild(obstacle)
				if obstacle.name == "obstacle4" && regions1.count > 0{
					addChild(generateRandomObstacle(&regions1, heightToSpawn: size.height, stage: stage))
					numberOfObstacles1++
				}
				if obstacle.name == "obstacle2"{
					triangle = obstacle
				}
			}
		}
		if numberOfObstacles1 < 5{
			if let triangle = triangle{
				let randomPosition = Int(arc4random_uniform(UInt32(regions1.count)))
				let freeSpot = CGPoint(x: CGFloat(regions1[randomPosition]), y: size.height)
				sendTriangleToPatrol(triangle, freeSpot: freeSpot)
			}
		}
		//Segunda onda
		let numberOfObstacles2 = arc4random_uniform(3) + 2
		var regions2 = [307.2, 537.6, 768, 998.4, 1228.8]
		var triangle2 : SKSpriteNode?
		for _ in 1...numberOfObstacles2{
			let obstacle = generateRandomObstacle(&regions2, heightToSpawn: size.height + distanceToSpawnObstacles/2, stage: stage)
			addChild(obstacle)
			if obstacle.name == "obstacle2"{
				triangle2 = obstacle
			}
		}
		if let triangle2 = triangle2{
			let randomPosition = Int(arc4random_uniform(UInt32(regions2.count)))
			let freeSpot = CGPoint(x: CGFloat(regions2[randomPosition]), y: size.height + distanceToSpawnObstacles/2)
			sendTriangleToPatrol(triangle2, freeSpot: freeSpot)
		}
		//MODIFICADO, AGORA A CHANCE É MENOR, N SEI QUANTO ------------!--------
		//Spawnando obstáculo bonus com 36% de chance a cada 2 ondas
		if(regions1.count > 0 && (regions1.count <= 2 && regions2.count <= 2)){
			var regionsBonusRight = [0.5625, 0.6875, 0.8125]
			var regionsBonusLeft = [0.1875, 0.3125, 0.4375]
			var regionBonus = [0.1875, 0.3125, 0.4375, 0.5625, 0.6875, 0.8125]
			var x : CGFloat = CGFloat(regionBonus[Int(arc4random_uniform(UInt32(6)))])
			let randomIndex = Int(arc4random_uniform(UInt32(3)))
			//Passagem mais a direita da primeira é a mais a direita da segunda
			if regions1.last == regions2.last {
				if regions1.last < 0.5 {
					x = CGFloat(regionsBonusRight[randomIndex])
				}else{
					x = CGFloat(regionsBonusLeft[randomIndex])
				}
			}
				//Passagem mais a direita da primeira está a esquerda da mais a direita da segunda
			else if regions1.last < regions2.last {
				if regions2.last != 0.8125{
					x = CGFloat(regionsBonusRight.last)
				}else if regions2.last == 0.8125 && regions1[0] != 0.1875{
					x = CGFloat(regionsBonusLeft[0])
				}
			}
				//Passagem mais a direita está a direita da mais a direita da segunda
			else if regions2.last < regions1.last {
				if regions2[0] != 0.1875{
					x = CGFloat(regionsBonusLeft[0])
				}else if regions2[0] == 0.1875 && regions1.last != 0.8125{
					x = CGFloat(regionsBonusRight.last)
				}
			}
			let newObstacleBonus = createBonus(size)
			newObstacleBonus.position = CGPoint(x: size.width * x, y: size.height + newObstacleBonus.size.height/2 + 250)
			addChild(newObstacleBonus)
		}
	}
	
	func configBackbround(){
		
		backgroundNode.anchorPoint = CGPointZero
		backgroundNode.name = "background"
		//backgroundNode.position = CGPoint(x: 0 ,y: CGFloat(i)*background.size.height )
		backgroundNode.zPosition = -1
		backgroundNode.size = CGSize(width: 1536, height: 2048*2)
		addChild(backgroundNode)
		
		/* background0 */
		background0.anchorPoint = CGPointZero
		background0.position = CGPoint(x: 0, y: 0)
		background0.name = "background0"
		background0.size = CGSize(width: 1536, height: 2048)
		backgroundNode.addChild(background0)
		
		/* background1 */
		background1.anchorPoint = CGPointZero
		background1.position = CGPoint(x: 0, y: 2048)
		background1.name = "background1"
		background1.size = CGSize(width: 1536, height: 2048)
		backgroundNode.addChild(background1)
	}
	/* criar background com várias imagens */
	func updateBackground(){
		background0.texture = backgrounds[background]
		background1.texture = backgrounds[background]
	}
	/* mover background, sem atrasos do update() */
	func moveNodes() {
		
		let backgroundVelocity = CGPoint(x: 0, y: -self.backgroundMovePointsPerSec)
		let amountToMove = backgroundVelocity * CGFloat(self.dt)
		//Moving background
		enumerateChildNodesWithName("background") { node, _ in
			let background = node as! SKSpriteNode
			background.position += amountToMove
			if background.position.y <= -background.size.height/2 {
				background.position.y = 0
			}
		}
		//Moving obstacles
		enumerateChildNodesWithName("obstacle[1-5]") { node, _ in
			let obstacle = node as! SKSpriteNode
			if obstacle.position.y + obstacle.size.height/2 < 0{
				obstacle.removeFromParent()
			}
			else {
				obstacle.position += amountToMove
			}
		}
		//Moving bonuses
		enumerateChildNodesWithName("bonus") { node, _ in
			let bonusNode = node as! SKSpriteNode
			if bonusNode.position.y + bonusNode.size.height/2 < 0{
				bonusNode.removeFromParent()
			}
			else {
				bonusNode.position += amountToMove
			}
		}
		//Moving particles
		enumerateChildNodesWithName("particle") { node, _ in
			let particle = node as! SKEmitterNode
			if particle.position.y < 0{
				particle.removeFromParent()
			}
			else {
				particle.position += amountToMove
			}
		}
		//Moving ball
		ballSprite.position += amountToMove			//Move touched position
		lastTouchedPosition += amountToMove
		shapeNode.position += amountToMove
		childNodeWithName("ballConcentrating")?.position = ballSprite.position
	}
	
	func untransformBall() {
		ballType = "ballDefault"
		ballSprite.typeOfBall(ballType)
		ballSprite.removeAllChildren()
		ballSprite.trail?.targetNode = self
		enumerateChildNodesWithName("chosenBonus") { node, _ in
			let bonusNode = node as! SKSpriteNode
			bonusNode.removeFromParent()
		}
	}
	
	/* código velocidade background */
	func updateVelocity() {
		if backgroundMovePointsPerSec > 0{
			/* alterando pontuação */
			scoreValue++
			score.text = String(scoreValue)
			if backgroundMovePointsPerSec < maxVelocityOfStage {
				backgroundMovePointsPerSec += 0.1
			}
		}
	}
	
	/* menu pausa */
	override func pauseGame() {
		
		/* para a bola quando o jogo entra em pausa */
		ballSprite.scene?.paused = true
		
		buttonHomeMenuPauseActive = true
		buttonSoundMenuPauseActive = true
		screenMenuPauseActive = true
		pauseOfGameActive = true
		
		/* desabilita o pause da tela do jogo */
		buttonPause.userInteractionEnabled = true
		
		/* configura screen do menu pausa */
		screenMenuPause.size = CGSize(width: size.width, height: size.height)
		screenMenuPause.position = CGPoint(x: size.width/2, y: size.height/2)
		screenMenuPause.zPosition = 200
		addChild(screenMenuPause)
		
		/* configura buttonSound do menu pausa */
		if isSoundOn {
			buttonSound = SKSpriteNode(imageNamed: "soundOnFilled")
		}
		else {
			buttonSound = SKSpriteNode(imageNamed: "soundOffFilled")
		}
		buttonSound.size = CGSize(width: 200, height: 200)
		buttonSound.position = CGPoint(x: size.width/2, y: size.height/2 + 350)
		buttonSound.zPosition = 201
		addChild(buttonSound)
		
		/* configura buttonHome do menu pausa */
		buttonHomeMenuPause.size = CGSize(width: 200, height: 200)
		buttonHomeMenuPause.position = CGPoint(x: size.width/2, y: 250)
		buttonHomeMenuPause.zPosition = 201
		addChild(buttonHomeMenuPause)
		
		/* configura a label do menu pausa */
		labelMenuPause.text = NSLocalizedString("TapToContinue", comment: "Toque para continuar")
		labelMenuPause.color = UIColor.whiteColor()
		labelMenuPause.colorBlendFactor = 1
		labelMenuPause.fontSize = 90
		labelMenuPause.position = CGPoint(x: size.width/2, y: size.height/2)
		labelMenuPause.zPosition = 201
		addChild(labelMenuPause)
	}
	
	/* buttonHome das telas do menu pause e game over */
	func actionButtonHome() {
		//iad
		verificationScreenAd = true
		
		if isSoundOn {
			doVolumeFadeOut()
			audioPlayer.volume = 0.0
			audioPlayer.stop()
		}
		ballSprite.trail?.removeFromParent()
		buttonHomeMenuPauseActive = false
		
		if let controller = self.view?.window?.rootViewController as? GameViewController {
			controller.gameScene = MainMenu(size: size, sound: isSoundOn, vibration: vibrationOn, iAd: iAdOn)
			controller.gameScene.scaleMode = scaleMode
			let reveal = SKTransition.fadeWithColor(UIColor(red: 0.933, green: 0.866, blue: 0.752, alpha: 1.0), duration: 1.0)
			view?.presentScene(controller.gameScene , transition: reveal)
		}
		
	}
	
	/* buttonRestart da tela menu game over */
	func actionButtonRestart() {
		if isSoundOn {
			doVolumeFadeOut()
			audioPlayer.volume = 0.0
			audioPlayer.stop()
		}
		ballSprite.trail?.removeFromParent()
		buttonRestartMenuGameOverActive = false
		
		if let controller = self.view?.window?.rootViewController as? GameViewController {
			controller.gameScene = GameScene(size: size, sound: isSoundOn, vibration: vibrationOn, iAd: iAdOn)
			controller.gameScene.scaleMode = scaleMode
			let reveal = SKTransition.fadeWithColor(UIColor(red: 0.933, green: 0.866, blue: 0.752, alpha: 1.0), duration: 1.0)
			//Retorna pro primeiro background
			background = "backgroundGame"
			view?.presentScene(controller.gameScene , transition: reveal)
		}
	}
	
	/* usada ao tocar na tela do menu pause */
	func menuPause() {
		ballSprite.scene?.paused = false
		buttonHomeMenuPauseActive = false
		buttonSoundMenuPauseActive = false
		pauseOfGameActive = false
		buttonPause.userInteractionEnabled = false
		buttonHomeMenuPause.removeFromParent()
		buttonSound.removeFromParent()
		screenMenuPause.removeFromParent()
		labelMenuPause.removeFromParent()
	}
	
	/* menu game over */
	func death() {
		//Imprensado
		//Som da morte
		if isSoundOn {
			runAction(SKAction.playSoundFileNamed("effectDeath.mp3", waitForCompletion:false))
		}
		//Vibrar uma vez
		if vibrationOn {
			AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
		}
		//Explosão
		addChild(createExplosionBall(ballType, ballSprite: ballSprite))
		ballSprite.removeAllChildren()
		ballSprite.removeFromParent()
		//1 segundo depois gameOver
		runAction(SKAction.waitForDuration(1.0), completion: {
			self.gameOver()
		})
		
		//iAds
		if iAdOn! {
			self.loadInterstitialAd()
		}
		gameOverActive = true
		backgroundMovePointsPerSec = 0.0
		
	}
	
	func gameOver() {
		
		//Achievement
		if !ballMoved {
			idleBall = true
		}
		
		//Remover partículas da tela
		enumerateChildNodesWithName("particle") { node, _ in
			let particle = node as! SKEmitterNode
			particle.removeFromParent()
		}
		ballSprite.removeAllChildren()
		/* desabilita o pause da tela do jogo */
		//verificar se está funcionando
		buttonPause.userInteractionEnabled = true
		gameOverActive = true
		buttonHomeMenuGameOverActive = true
		buttonRestartMenuGameOverActive = true
		/* verifica se é a maior pontuação */
		if(scoreValue > highScore){
			highScore = scoreValue
			saveHighScore()
			
			//Salva highscore no GameCenter
			reportScoreToGameCenter()
		}
		/* configura screen do menu gameOver*/
		screenMenuGameOver.size = CGSize(width: size.width, height: size.height)
		screenMenuGameOver.position = CGPoint(x: size.width/2, y: size.height/2)
		screenMenuGameOver.zPosition = 200
		addChild(screenMenuGameOver)
		
		labelMenuGameOver.text = NSLocalizedString("GameOver", comment: "Fim de jogo!")
		labelMenuGameOver.color = UIColor.whiteColor()
		labelMenuGameOver.colorBlendFactor = 1
		labelMenuGameOver.fontSize = 120
		labelMenuGameOver.position = CGPoint(x: size.width/2, y: size.height/2+620)
		labelMenuGameOver.zPosition = 201
		addChild(labelMenuGameOver)
		
		let scoreLabel : String = NSLocalizedString("Score", comment: "Pontuação")
		if let scoreString = score.text{
			labelScoreMenuGameOver.text = "\(scoreLabel): \(scoreString)"
		}
		//labelScoreMenuGameOver.text = "\(scoreLabel): \(score.text)"
		labelScoreMenuGameOver.color = UIColor.whiteColor()
		labelScoreMenuGameOver.colorBlendFactor = 1
		labelScoreMenuGameOver.fontSize = 70
		labelScoreMenuGameOver.position = CGPoint(x: size.width/2, y: size.height/2+420)
		labelScoreMenuGameOver.zPosition = 201
		addChild(labelScoreMenuGameOver)
		
		let highScoreLabel : String = NSLocalizedString("HighScore", comment: "Maior Pontuação")
		labelHighScoreMenuGameOver.text = "\(highScoreLabel): \(highScore!)"
		labelHighScoreMenuGameOver.color = UIColor.whiteColor()
		labelHighScoreMenuGameOver.colorBlendFactor = 1
		labelHighScoreMenuGameOver.fontSize = 70
		labelHighScoreMenuGameOver.position = CGPoint(x: size.width/2, y: size.height/2+320)
		labelHighScoreMenuGameOver.zPosition = 201
		addChild(labelHighScoreMenuGameOver)
		
		buttonHomeMenuGameOver.size = CGSize(width: 200, height: 200)
		buttonHomeMenuGameOver.position = CGPoint(x: size.width/2, y: 250)
		buttonHomeMenuGameOver.zPosition = 201
		addChild(buttonHomeMenuGameOver)
		
		buttonRestartMenuGameOver.size = CGSize(width: 500, height: 500)
		buttonRestartMenuGameOver.position = CGPoint(x: size.width/2, y: size.height/2 - 105)
		buttonRestartMenuGameOver.zPosition = 201
		addChild(buttonRestartMenuGameOver)
		
	}
	/* tempo bonus */
	func bonusTimeDuration(){
		let bonusTimeOfInt = Int(timeToUntransform)
		let bonusTimeOfString = "\(bonusTimeOfInt+1)" // "0.1"
		bonusTime.text =  bonusTimeOfString
	}
	
	func saveHighScore() {
		userDefaultManager.saveElement(highScore, forKey: "highScore")
	}
	
	func showTapAtLocation(point: CGPoint) {
		// Escolhendo a forma e cor cor do efeito
		let path = UIBezierPath(ovalInRect: CGRect(x: -15, y: -15, width: 30, height: 30))
		let color : SKColor = SKColor(red: 255, green: 255, blue: 255, alpha: 196)
		
		shapeNode = SKShapeNode()
		shapeNode.path = path.CGPath
		shapeNode.position = point
		shapeNode.strokeColor = color
		shapeNode.lineWidth = 1.5
		shapeNode.antialiased = false
		shapeNode.zPosition = 50
		addChild(shapeNode) //PODE SER REVISTA
		
		let duration = 0.6
		let scaleAction = SKAction.scaleTo(6.0, duration: duration)
		scaleAction.timingMode = .EaseOut
		shapeNode.runAction(SKAction.sequence([scaleAction, SKAction.removeFromParent()]))
		
		let fadeAction = SKAction.fadeOutWithDuration(duration)
		fadeAction.timingMode = .EaseOut
		shapeNode.runAction(fadeAction)
	}
	
	//Faz os efeitos das transformações na tela
	func transformationAuraActions(element : String){
		let (emitter1, emitter2) = createGigaExplosion(ballSprite, ballType: element)
		addChild(emitter1); addChild(emitter2)
		enumerateChildNodesWithName("obstacle[1-5]") { node, _ in
			let xDist = (node.position.x - self.ballSprite.position.x);
			let yDist = (node.position.y - self.ballSprite.position.y);
			let distance = sqrt((xDist * xDist) + (yDist * yDist));
			if distance < 750{
				if element == "ballFire"{
					//explodir obstáculo
					self.addChild(createExplosionBox(node))
					node.removeFromParent()
					//Achievement
					self.blocksBroken++
					scoreObstacle(bonusForObstacle(node), position: node.position, scene: self)
				}
				else if element == "ballGhost"{
					node.physicsBody!.categoryBitMask = PhysicsCategory.None
					node.runAction(SKAction.fadeOutWithDuration(0.5), completion: {
						node.removeFromParent()
						scoreObstacle(bonusForObstacle(node)/2, position: node.position, scene: self)
					})
				}
					
				else if element == "ballWater"{
					//Se o obstáculo for de fogo, apaga
					if node.name == "obstacle4"{
						node.removeAllChildren()
						let smoke = createSmokeObstacle()
						node.addChild(smoke)
						smoke.runAction(SKAction.waitForDuration(0.7), completion: { () -> Void in
							smoke.removeFromParent()
						})
						node.name = "obstacle3"
						scoreObstacle(bonusForObstacle(node)/2, position: node.position, scene: self)
						if self.isSoundOn {
							self.runAction(SKAction.playSoundFileNamed("effectPsst.mp3", waitForCompletion:false))
						}
					}
					//Iguala a massa dos obstáculos para empurrar igualmente
					node.physicsBody?.mass = 2.35
					node.physicsBody?.dynamic = true
					node.physicsBody?.collisionBitMask = PhysicsCategory.None
					let actionPush = SKAction.runBlock({ () -> Void in
						if node.position.x < self.ballSprite.position.x{
							node.physicsBody?.applyImpulse(CGVector(dx: -3000, dy: 0))
						}else{
							node.physicsBody?.applyImpulse(CGVector(dx: 3000, dy: 0))
						}
					})
					let actionWait = SKAction.waitForDuration(0.5)
					let actionBackToNormal = SKAction.runBlock({ () -> Void in
						node.physicsBody?.dynamic = false
						node.physicsBody?.collisionBitMask = PhysicsCategory.Ball
					})
					let sequence = SKAction.sequence([actionPush, actionWait, actionBackToNormal])
					node.runAction(sequence)
				}
			}
		}
	}
	
	//Ativa e desativa o carregamento do MegaMan
	func concentratingAura(on: Bool){
		if on && !ballIsConcentrating{
			ballIsConcentrating = true
			let concentratingAura = createConcentratingAura()
			concentratingAura.name = "concentratingAura"
			ballSprite.addChild(concentratingAura)
			let timeRelativeToVel = Double(1.6 - (backgroundMovePointsPerSec/1000))  //IMPLEMENTANDO
			let actionScale = SKAction.scaleBy(0.2, duration: timeRelativeToVel)
			let fadeToWhite = SKAction.group([
				SKAction.runAction(SKAction.fadeOutWithDuration(timeRelativeToVel), onChildWithName: "ballSprite"),
				SKAction.runAction(SKAction.fadeInWithDuration(timeRelativeToVel/2), onChildWithName: "ballConcentrating"), // Slower fade in
				])
			let wait = SKAction.waitForDuration(timeRelativeToVel - 0.1)
			let ballConcentrating = SKSpriteNode(texture: ballConcentratingTexture, size: size)
			ballConcentrating.alpha = 0.0 // Don't show at start
			ballConcentrating.name = "ballConcentrating"
			ballConcentrating.size = CGSize(width: 150, height: 150)
			ballConcentrating.position = ballSprite.position
			addChild(ballConcentrating)
			//Ação de completar o carregamento
			let chargingCompleted = SKAction.runBlock({ () -> Void in
				ballConcentrating.addChild(createCompletedCharge(self.ballType))
				self.ballChargedType = self.ballType
				//Remove as shurikens grudadas na bola
				self.enumerateChildNodesWithName("ballSprite/obstacle5") { node, _ in
					node.removeFromParent()
				}
				if self.isSoundOn {
					self.runAction(SKAction.playSoundFileNamed("effectConcentrated.mp3", waitForCompletion:false))
				}
			})
			runAction(SKAction.sequence([fadeToWhite, wait, chargingCompleted]), withKey: "fade")
			concentratingAura.runAction(actionScale, withKey: "actionScale")
			if isSoundOn {
				soundConcentrating.play()
			}
		} else if !on {
			ballIsConcentrating = false
			childNodeWithName("ballConcentrating")?.removeFromParent()
			let concentratingAura = ballSprite.childNodeWithName("concentratingAura")
			concentratingAura?.removeActionForKey("actionScale")
			concentratingAura?.removeFromParent()
			removeActionForKey("fade")
			ballSprite.removeAllActions()
			ballSprite.xScale = 1.0
			ballSprite.yScale = 1.0
			if ballType == "ballGhost"{
				ballSprite.alpha = 0.5
			}else{
				ballSprite.alpha = 1.0
			}
			if isSoundOn {
				soundConcentrating.stop()
				soundConcentrating.currentTime = 0
			}
		}
	}
	
	//Achievements
	func reportAchievements() {
		
		var achievements = [GKAchievement]()
		
		if blocksBroken==10  { achievements.append(AchievementsHelper.breakerAchievement(blocksBroken)) }
		if blocksBroken==50  { achievements.append(AchievementsHelper.crusherAchievement(blocksBroken)) }
		if blocksBroken==200 { achievements.append(AchievementsHelper.destroyerAchievement(blocksBroken)) }
		
		if scoreValue==5000  { achievements.append(AchievementsHelper.walkerAchievement(scoreValue)) }
		if scoreValue==10000 { achievements.append(AchievementsHelper.runnerAchievement(scoreValue)) }
		if scoreValue==20000 { achievements.append(AchievementsHelper.marathonerAchievement(scoreValue)) }
		
		if bonusesTaken==10  { achievements.append(AchievementsHelper.mageAchievement(bonusesTaken)) }
		if bonusesTaken==25  { achievements.append(AchievementsHelper.wizardAchievement(bonusesTaken)) }
		if bonusesTaken==50  { achievements.append(AchievementsHelper.archMageAchievement(bonusesTaken)) }
		
		if  fireBonusesTaken==10 { achievements.append(AchievementsHelper.fireMasterAchievement(fireBonusesTaken)) }
		if ghostBonusesTaken==10 { achievements.append(AchievementsHelper.ghostMasterAchievement(ghostBonusesTaken)) }
		if   gumBonusesTaken==10 { achievements.append(AchievementsHelper.gumMasterAchievement(gumBonusesTaken)) }
		if  leadBonusesTaken==10 { achievements.append(AchievementsHelper.leadMasterAchievement(leadBonusesTaken)) }
		if waterBonusesTaken==10 { achievements.append(AchievementsHelper.waterMasterAchievement(waterBonusesTaken)) }
		
		if idleBall { achievements.append(AchievementsHelper.idleBallAchievement()) }
		if iDontNeedHelp { achievements.append(AchievementsHelper.iDontNeedHelpAchievement()) }
		
		GameKitHelper.sharedInstance.reportAchievements(achievements)
		
	}
	
	func reportScoreToGameCenter() {
		GameKitHelper.sharedInstance.reportScore(Int64(scoreValue), forLeaderBoardId: "com.leandro.ProjectBall.Highscore")
	}
	
	//iAds
	func loadInterstitialAd() {
		interstitial = ADInterstitialAd()
		interstitial!.delegate = self
	}
	func interstitialAdDidUnload(interstitialAd: ADInterstitialAd!) {
		//self.interstitial?.loaded
	}
	func interstitialAd(interstitialAd: ADInterstitialAd!, didFailWithError error: NSError!) {
		//self.interstitial?.loaded
	}
	func interstitialAdActionShouldBegin(interstitialAd: ADInterstitialAd!, willLeaveApplication willLeave: Bool) -> Bool {
		return true
	}
	func interstitialAdActionDidFinish(interstitialAd: ADInterstitialAd!) {
		if (!isSoundOn &&  verificationOfSoundAd == true ){
			
			audioPlayer.volume = 0.0
			doVolumeFadeIn()
			isSoundOn = true
			verificationOfSoundAd = false
		}
		self.interstitialAdView.hidden = true
		
	}
	func interstitialAdDidLoad(interstitialAd: ADInterstitialAd!) {
		
		if isSoundOn {
			//Código para desativar o som de background
			doVolumeFadeOut()
			isSoundOn = false
			verificationOfSoundAd = true
		}
		if verificationScreenAd == false{
			interstitialAdView = UIView()
			interstitialAdView.frame = self.view!.bounds
			view!.addSubview(interstitialAdView)
			interstitialAd.presentInView(interstitialAdView)
			UIViewController.prepareInterstitialAds()
		}
	}
}

