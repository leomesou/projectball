//
//  Tutorial.swift
//  ProjectBall
//
//  Created by Rodrigo Bruno Mendes Theóphilo on 14/07/15.
//  Copyright (c) 2015 LPR. All rights reserved.
//

import SpriteKit

private var firstTutorialCompleted = false
private var secondTutorialCompleted = false
private var actualTutorial = 1

enum TutorialType {
	case MoveBall
	case LongPress
	case GameplayDescription
}

func startTutorials(scene : GameScene) {
	scene.backgroundMovePointsPerSec = 0.0
	tutorialMoveBall(scene)
}

private func endTutorials(scene : GameScene) {
	scene.backgroundMovePointsPerSec = 200.0
}

private func tutorialMoveBall(scene : GameScene) {
	//Dedo clicando
	let finger = SKSpriteNode(imageNamed: "finger")
	finger.anchorPoint = CGPointZero
	finger.position = CGPoint(x: scene.size.width/2, y: scene.size.height - 200)
	finger.xScale = 2; finger.yScale = 2;
	
	let moveDown = SKAction.moveByX(0, y: -200, duration: 0.5)
	let showClick = SKAction.runBlock { () -> Void in
		scene.showTapAtLocation(finger.position)
	}
	let backToInitialPosition = SKAction.runBlock { () -> Void in
		finger.position = CGPoint(x: scene.size.width/2, y: scene.size.height - 200)
	}
	let fingerClick = SKAction.sequence([moveDown, showClick, SKAction.waitForDuration(0.5), backToInitialPosition])
	let fingerClicking = SKAction.repeatActionForever(fingerClick)
	finger.name = "tutorialFinger"
	scene.addChild(finger)
	finger.runAction(fingerClicking, withKey: "tutorialFingerAction")
	//Label explicando
	let label = SKLabelNode(fontNamed: fontName)
	label.fontColor = SKColor.blackColor()
	label.text = NSLocalizedString("TapToMove", comment: "TAP TO MOVE")
	label.fontSize = 80
	label.position = CGPoint(x: scene.size.width/2, y: scene.size.height-550)
	label.name = "tutorialLabel"
	scene.addChild(label)
}

private func tutorialLongPress(scene : GameScene) {
	let finger = SKSpriteNode(imageNamed: "finger")
	finger.anchorPoint = CGPointZero
	finger.position = CGPoint(x: scene.size.width/2, y: scene.size.height - 200)
	finger.xScale = 2; finger.yScale = 2;
	
	let moveDown = SKAction.moveByX(0, y: -200, duration: 0.5)
	let showClick = SKAction.runBlock { () -> Void in
		scene.showTapAtLocation(finger.position)
	}
	let wait = SKAction.waitForDuration(0.5)
	let moveUp = SKAction.moveByX(0, y: 200, duration: 0.25)
	let backToInitialPosition = SKAction.runBlock { () -> Void in
		finger.position = CGPoint(x: scene.size.width/2, y: scene.size.height - 200)
	}
	let fingerClick = SKAction.sequence([moveDown, showClick, wait, showClick, wait, showClick, wait, showClick, moveUp, wait, backToInitialPosition])
	let fingerClicking = SKAction.repeatActionForever(fingerClick)
	finger.name = "tutorialFinger"
	scene.addChild(finger)
	finger.runAction(fingerClicking, withKey: "tutorialFingerAction")
	
	//Label explicando
	let label = SKLabelNode(fontNamed: fontName)
	label.fontColor = SKColor.blackColor()
	label.text = NSLocalizedString("HoldAndReleaseToBreak", comment: "HOLD AND RELEASE TO BREAK!")
	label.fontSize = 52
	label.position = CGPoint(x: scene.size.width/2, y: scene.size.height-550)
	label.name = "tutorialLabel"
	scene.addChild(label)
	
	//Adiciona bloco para ser quebrado
	let block = createObstacle("block")
	block.name = "tutorialBlock"
	block.position = CGPoint(x: scene.size.width/2, y: scene.size.height/2-150)
	scene.addChild(block)
	
}

private func tutorialGameplayDescription(scene : GameScene) {
	let backgroundTutorial = SKSpriteNode(texture: scene.screenMenuPause.texture)
	backgroundTutorial.size = CGSize(width: 1536, height: 2048)
	backgroundTutorial.anchorPoint = CGPointZero
	backgroundTutorial.alpha = 0
	backgroundTutorial.zPosition = 1
	backgroundTutorial.name = "tutorialBackground"
	
	//Label explicando
	let label1 = SKLabelNode(fontNamed: fontName)
	label1.text = NSLocalizedString("ChangeYourProperty", comment: "CHANGE YOUR PROPERTY")
	label1.fontSize = 60
	label1.position = CGPoint(x: scene.size.width/2, y: scene.size.height-300)
	label1.alpha = 0
	backgroundTutorial.addChild(label1)
	
	let label2 = SKLabelNode(fontNamed: fontName)
	label2.text = NSLocalizedString("Absorbing", comment: "ABSORBING")
	label2.fontSize = 60
	label2.position = CGPoint(x: scene.size.width/2, y: label1.position.y - 160)
	label2.alpha = 0
	backgroundTutorial.addChild(label2)
	
	let bonus1 = SKSpriteNode(texture: obstaclesBonus["lead"], color:UIColor.clearColor(), size:CGSize(width: scene.size.width*0.08, height: scene.size.width*0.08))
	bonus1.position = CGPoint(x: scene.size.width/2 - 400, y: label2.position.y - 160)
	bonus1.alpha = 0
	backgroundTutorial.addChild(bonus1)
	
	let bonus2 = SKSpriteNode(texture: obstaclesBonus["water"], color:UIColor.clearColor(), size:CGSize(width: scene.size.width*0.08, height: scene.size.width*0.08))
	bonus2.position = CGPoint(x: scene.size.width/2 - 200, y: label2.position.y - 160)
	bonus2.alpha = 0
	backgroundTutorial.addChild(bonus2)
	
	let bonus3 = SKSpriteNode(texture: obstaclesBonus["fire"], color:UIColor.clearColor(), size:CGSize(width: scene.size.width*0.08, height: scene.size.width*0.08))
	bonus3.position = CGPoint(x: scene.size.width/2, y: label2.position.y - 160)
	bonus3.alpha = 0
	backgroundTutorial.addChild(bonus3)
	
	let bonus4 = SKSpriteNode(texture: obstaclesBonus["ghost"], color:UIColor.clearColor(), size:CGSize(width: scene.size.width*0.08, height: scene.size.width*0.08))
	bonus4.position = CGPoint(x: scene.size.width/2 + 200, y: label2.position.y - 160)
	bonus4.alpha = 0
	backgroundTutorial.addChild(bonus4)
	
	let bonus5 = SKSpriteNode(texture: obstaclesBonus["gum"], color:UIColor.clearColor(), size:CGSize(width: scene.size.width*0.08, height: scene.size.width*0.08))
	bonus5.position = CGPoint(x: scene.size.width/2 + 400, y: label2.position.y - 160)
	bonus5.alpha = 0
	backgroundTutorial.addChild(bonus5)
	
	//TERCEIRO TEXTO
	let label3 = SKLabelNode(fontNamed: fontName)
	label3.text = NSLocalizedString("DiscoverDifferentBehaviors", comment: "DISCOVER DIFFERENT BEHAVIORS")
	label3.fontSize = 50
	label3.position = CGPoint(x: scene.size.width/2, y: bonus1.position.y - 210)
	label3.alpha = 0
	backgroundTutorial.addChild(label3)
	
	let label4 = SKLabelNode(fontNamed: fontName)
	label4.text = NSLocalizedString("AndRunToLive", comment: "AND RUN TO LIVE!")
	label4.fontSize = 75
	label4.position = CGPoint(x: scene.size.width/2, y: label3.position.y - 300)
	label4.alpha = 0
	backgroundTutorial.addChild(label4)
	
	let fadeInLabel = SKAction.fadeInWithDuration(1)
	let fadeOutLabel = SKAction.fadeOutWithDuration(1)
	let fadeInBonus = SKAction.fadeInWithDuration(0.5)
	let fadeOutBonus = SKAction.fadeOutWithDuration(2)
	
	let waitForLabel = SKAction.waitForDuration(1)
	let waitForBonus = SKAction.waitForDuration(0.5)
	let longWait = SKAction.waitForDuration(2.0)
	//LABELS
	let fadeLabel1 = SKAction.runBlock { () -> Void in
		label1.runAction(fadeInLabel)
	}
	let fadeLabel2 = SKAction.runBlock { () -> Void in
		label2.runAction(fadeInLabel)
	}
	let fadeLabel3 = SKAction.runBlock { () -> Void in
		label3.runAction(fadeInLabel)
	}
	let fadeOutLabel3 = SKAction.runBlock { () -> Void in
		label3.runAction(fadeOutLabel)
	}
	let fadeInLabel3new = SKAction.runBlock { () -> Void in
		label3.text = NSLocalizedString("SomeHaveDifferentPowers", comment: "SOME HAVE DIFFERENT POWERS (HOLD)")
		label3.runAction(fadeInLabel)
	}
	let fadeLabel4 = SKAction.runBlock { () -> Void in
		label4.runAction(fadeInLabel)
	}
	//BONUS
	let fadeBonus1 = SKAction.runBlock { () -> Void in
		bonus1.runAction(fadeInBonus)
	}
	let fadeBonus2 = SKAction.runBlock { () -> Void in
		bonus2.runAction(fadeInBonus)
	}
	let fadeBonus3 = SKAction.runBlock { () -> Void in
		bonus3.runAction(fadeInBonus)
	}
	let fadeBonus4 = SKAction.runBlock { () -> Void in
		bonus4.runAction(fadeInBonus)
	}
	let fadeBonus5 = SKAction.runBlock { () -> Void in
		bonus5.runAction(fadeInBonus)
	}
	let fadeOutChargelessBonus = SKAction.runBlock { () -> Void in
		bonus1.runAction(fadeOutBonus)
		bonus5.runAction(fadeOutBonus)
	}
	
	let firstLabelsSequence = SKAction.sequence([fadeLabel1, waitForLabel, fadeLabel2, waitForLabel])
	let bonusSequence = SKAction.sequence([fadeBonus1, waitForBonus, fadeBonus2, waitForBonus, fadeBonus3, waitForBonus, fadeBonus4, waitForBonus, fadeBonus5, waitForBonus])
	let lastLabelsSequence = SKAction.sequence([fadeLabel3, waitForLabel, fadeOutChargelessBonus, longWait, fadeOutLabel3, waitForLabel, fadeInLabel3new, longWait, fadeLabel4, waitForLabel, longWait])
	
	scene.addChild(backgroundTutorial)
	let backgroundFade = SKAction.fadeInWithDuration(0.5)
	
	backgroundTutorial.runAction(SKAction.sequence([backgroundFade, firstLabelsSequence, bonusSequence, lastLabelsSequence]), completion: { () -> Void in
		tutorialDone(TutorialType.GameplayDescription, scene: scene)
	})
}

func tutorialDone(tutorial : TutorialType, scene : GameScene) {
	//Retorna a bola pra posição inicial
	tutorialCleanScreen(tutorial, scene: scene)
	scene.isTutorialOn = false
	switch tutorial{
	case .MoveBall:
		firstTutorialCompleted = true
		tutorialStateAnimated(scene)
		actualTutorial = 2
		scene.runAction(SKAction.waitForDuration(2.0), completion: { () -> Void in
			//Remove o block caso não tenha sido destruído
			scene.childNodeWithName("tutorialBlock")?.removeFromParent()
			scene.ballSprite.position = CGPoint(x: scene.size.width/2, y: scene.size.height/2 - 400)
			scene.ballSprite.physicsBody?.velocity = CGVectorMake(0, 0)
			tutorialLongPress(scene)
			scene.isTutorialOn = true
		})
	case .LongPress:
		secondTutorialCompleted = true
		tutorialStateAnimated(scene)
		actualTutorial = 3
		scene.runAction(SKAction.waitForDuration(2.0), completion: { () -> Void in
			//Remove o block caso não tenha sido destruído
			scene.childNodeWithName("tutorialBlock")?.removeFromParent()
			scene.ballSprite.position = CGPoint(x: scene.size.width/2, y: scene.size.height/2 - 400)
			scene.ballSprite.physicsBody?.velocity = CGVectorMake(0, 0)
			tutorialGameplayDescription(scene)
		})
	case .GameplayDescription:
		endTutorials(scene)
	}
}

//Função auxiliar para mostrar "check" de estado de tutorial
private func tutorialStateAnimated(scene : GameScene) {
	let imageName : String
	if actualTutorial == 1{
		if firstTutorialCompleted{
			imageName = "right-check"
		}else{
			imageName = "wrong-check"
		}
	}else{
		if secondTutorialCompleted{
			imageName = "right-check"
		}else{
			imageName = "wrong-check"
		}
	}
	let check = SKSpriteNode(imageNamed: imageName)
	check.size = CGSizeMake(500, 500)
	check.zPosition = 60
	check.position = CGPoint(x: scene.size.width/2, y: scene.size.height/2)
	check.xScale = 0; check.yScale = 0;
	scene.addChild(check)
	let scale = SKAction.scaleTo(1, duration: 0.25)
	let wait = SKAction.waitForDuration(1.75)
	scale.timingMode = SKActionTimingMode.EaseOut
	check.runAction(SKAction.sequence([scale, wait]), completion: { () -> Void in
		check.removeFromParent()
	})
}

private func tutorialCleanScreen(tutorial : TutorialType, scene : GameScene){
	switch tutorial{
	case .MoveBall , .LongPress:
		scene.removeActionForKey("tutorialFingerAction")
		scene.childNodeWithName("tutorialFinger")?.removeFromParent()
		scene.childNodeWithName("tutorialLabel")?.removeFromParent()
	case .GameplayDescription:
		if let backgroundTutorial = scene.childNodeWithName("tutorialBackground") as? SKSpriteNode{
			backgroundTutorial.removeAllChildren()
			backgroundTutorial.runAction(SKAction.fadeOutWithDuration(0.5), completion: { () -> Void in
				backgroundTutorial.removeFromParent()
				scene.userDefaults.setValue(scene.isTutorialOn, forKey: "isTutorialOn")
				scene.userDefaults.synchronize()
			})
		}
	}
	
}
