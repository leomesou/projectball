//
//  UserDefaultManager.swift
//  ProjectBall
//
//  Created by Rodrigo Bruno on 10/11/15.
//  Copyright © 2015 LPR. All rights reserved.
//
import UIKit
/* Todo mundo que quiser salvar pode */
let userDefaultManager = UserDefaultManager()

class UserDefaultManager : NSObject{
	private let manager = NSUserDefaults.standardUserDefaults()
	
	// MARK: - Load Methods
	
	func retrieveElementForKey(key: String) -> AnyObject?{
		return manager.objectForKey(key)
	}
	
	// MARK: - Save Methods
	
	func saveElement(element: AnyObject?, forKey key: String){
		if let element = element{
			switch element{
			case let intElement as Int:
				manager.setInteger(intElement, forKey: key)
			case let boolElement as Bool:
				manager.setBool(boolElement, forKey: key)
			case let floatElement as Float:
				manager.setFloat(floatElement, forKey: key)
			case let nsDataElement as NSData:
				manager.setObject(nsDataElement, forKey: key)
			default:
				manager.setValue(element, forKey: key)
			}
		}
	}
	
	// MARK: - Reset
	func resetGameSave(){
		/* Antes de deletar tudo, recupere as configuracoes dos usuarios e guarde em variaveis temp */
		let highScore = retrieveElementForKey("highScore") as? Int
		let isSoundOn = retrieveElementForKey("isSoundOn") as? Bool
		
		/* Papoca todos os arquivos salvos no jogo */
		if let appDomainName = NSBundle.mainBundle().bundleIdentifier{
			manager.removePersistentDomainForName(appDomainName)
		}
		
		/* Logo depois devolva pro disco */
		saveElement(highScore, forKey: "highScore")
		saveElement(isSoundOn, forKey: "isSoundOn")
	}
}