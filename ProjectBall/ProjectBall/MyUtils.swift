//
//  MyUtils.swift
//  ProjectBall
//
//  Created by Pedro Vitor de Sousa Guimaraes on 20/03/15.
//  Copyright (c) 2015 LPR. All rights reserved.
//

import CoreGraphics

func + (v1: CGVector, v2: CGVector) -> CGVector {
	return CGVector(dx: v1.dx + v2.dx, dy: v1.dy + v2.dy)
}

func * (vector: CGVector, multiplier: CGFloat) -> CGVector {
	return CGVector(dx: vector.dx * CGFloat(multiplier), dy: vector.dy * CGFloat(multiplier))
}

func / (vector: CGVector, divisor: Double) -> CGVector {
	return CGVector(dx: vector.dx / CGFloat(divisor), dy: vector.dy / CGFloat(divisor))
}

func > (vector: CGVector, number: Double) -> Bool {
	return ((abs(vector.dx) > CGFloat(number)) || (abs(vector.dy) > CGFloat(number)))
}

func < (vector: CGVector, number: Double) -> Bool {
	return ((abs(vector.dx) < CGFloat(number)) || (abs(vector.dy) < CGFloat(number)))
}

func + (left: CGPoint, right: CGPoint) -> CGPoint {
	return CGPoint(x: left.x + right.x, y: left.y + right.y)
}

func += (inout left: CGPoint, right: CGPoint) {
	left = left + right
}

func - (left: CGPoint, right: CGPoint) -> CGPoint {
	return CGPoint(x: left.x - right.x, y: left.y - right.y)
}

func -= (inout left: CGPoint, right: CGPoint) {
	left = left - right
}

func * (left: CGPoint, right: CGPoint) -> CGPoint {
	return CGPoint(x: left.x * right.x, y: left.y * right.y)
}

func *= (inout left: CGPoint, right: CGPoint) {
	left = left * right
}

func * (point: CGPoint, scalar: CGFloat) -> CGPoint {
	return CGPoint(x: point.x * scalar, y: point.y * scalar)
}

func *= (inout point: CGPoint, scalar: CGFloat) {
	point = point * scalar
}

func / (left: CGPoint, right: CGPoint) -> CGPoint {
	return CGPoint(x: left.x / right.x, y: left.y / right.y)
}

func /= (inout left: CGPoint, right: CGPoint) {
	left = left / right
}

func / (point: CGPoint, scalar: CGFloat) -> CGPoint {
	return CGPoint(x: point.x / scalar, y: point.y / scalar)
}

func /= (inout point: CGPoint, scalar: CGFloat) {
	point = point / scalar
}

#if !(arch(x86_64) || arch(arm64))
	func atan2(y: CGFloat, x: CGFloat) -> CGFloat {
		return CGFloat(atan2f(Float(y), Float(x)))
	}
	func sqrt(a: CGFloat) -> CGFloat {
		return CGFloat(sqrtf(Float(a)))
	}
#endif
extension CGFloat {
	static func random() -> CGFloat {
		return CGFloat(Float(arc4random()) / Float(UInt32.max))
	}
	static func random(min min: CGFloat, max: CGFloat) -> CGFloat {
		assert(min < max)
		return CGFloat.random() * (max - min) + min
	}
}
extension CGPoint {
	func length() -> CGFloat {
		return sqrt(x*x + y*y)
	}
	func normalized() -> CGPoint {
		return self / length()
	}
	var angle: CGFloat {
		return atan2(y, x)
	}
}
extension CGVector {
	var angle: CGFloat {
		return atan2(dy, dx)
	}
}
extension Array {
	var last: Element {
		return self[self.endIndex - 1]
	}
}
