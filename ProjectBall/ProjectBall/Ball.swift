//
//  Ball.swift
//  ProjectBall
//
//  Created by Leandro on 17/03/15.
//  Copyright (c) 2015 LPR. All rights reserved.
//

import SpriteKit

class Ball: SKSpriteNode {
	
	var velocity : CGFloat?
	var trail : SKEmitterNode?
	
	var properties : [CGFloat] = []
	
	struct PhysicsCategory {
		static let None: UInt32 = 0
		static let Ball: UInt32 = 0b1 // 1
		static let Obstacle: UInt32 = 0b10 // 2
		static let Edge: UInt32 = 0b100 // 4
		static let ObstacleBonus: UInt32 = 0b1000 // 8
	}
	
	required init(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	init(imageName: String) {
		
		let ballTexture = SKTexture(imageNamed: imageName)
		super.init(texture:ballTexture, color:UIColor.clearColor(), size:ballTexture.size())
		self.name = "ballSprite"
		
		size.width  = 120
		size.height = 120
		
		anchorPoint.x = 0.5
		anchorPoint.y = 0.5
		
		physicsBody = SKPhysicsBody(circleOfRadius: size.width/2.5, center: position)
		
		//physicsBody!.usesPreciseCollisionDetection = true
		
		physicsBody!.allowsRotation = false
		typeOfBall(imageName)
	}
	
	func typeOfBall(imageName : String) {
		
		texture = SKTexture(imageNamed: imageName)
		
		zPosition = 1
		
		physicsBody!.categoryBitMask = PhysicsCategory.Ball
		physicsBody!.collisionBitMask = PhysicsCategory.Obstacle | PhysicsCategory.Edge
		physicsBody!.contactTestBitMask = PhysicsCategory.Obstacle | PhysicsCategory.Edge
		
		trail?.removeFromParent()
		
		velocityForBall(imageName)
		
		switch imageName {
		case "ballWater":
			alpha = CGFloat(1)
			properties = [ 1.00, 0.25, 0.70, 0.50]
			trail = createTrail("ballWater")
			addChild(trail!)
		case "ballFire" :
			alpha = CGFloat(1)
			properties = [ 1.50, 0.00, 0.25, 0.75]
			trail = createTrail("ballFire")
			addChild(trail!)
		case "ballGhost":
			alpha = CGFloat(0.5)
			properties = [ 0.50, 1.00, 1.00, 1.00]
			physicsBody!.collisionBitMask = PhysicsCategory.Edge
			physicsBody!.contactTestBitMask = PhysicsCategory.Edge
			zPosition = 2
			trail = createTrail("ballGhost")
			addChild(trail!)
		case "ballGum"  :
			alpha = CGFloat(1)
			properties = [ 0.75, 0.00, 0.00, 1.00]
		case "ballLead" :
			alpha = CGFloat(1)
			properties = [11.34, 0.50, 0.50, 0.00]
		default:
			alpha = CGFloat(1)
			properties = [ 2.00, 0.10, 0.10, 0.50]
		}
		
		physicsBody!.density = properties[0] //Determina como forças se aplicam no corpo
		physicsBody!.restitution = properties[1] //Energia que o corpo mantém depois da colisão
		physicsBody!.linearDamping = properties[2] //Perda de velocidade
		physicsBody!.friction = properties[3] //Força que o corpo aplica no outro na colisão
		
		physicsBody!.angularDamping = 0.0 //Perda de rotação
	}
	
	func velocityForBall(imageName : String){
		switch imageName {
		case "ballWater":
			velocity =  1000
		case "ballFire" :
			velocity = 1500
		case "ballGhost":
			velocity = 1250
		case "ballGum"  :
			velocity = 1200
		case "ballLead" :
			velocity =  800
		default:
			velocity = 1000
		}
	}
}
