//
//  MainMenu.swift
//  ProjectBall
//
//  Created by Pedro Vitor de Sousa Guimaraes on 22/03/15.
//  Copyright (c) 2015 LPR. All rights reserved.
//

import SpriteKit
import AVFoundation
import AudioToolbox


class MainMenu: GameSceneInterface {
	
	let ballWater = SKSpriteNode(imageNamed: "water")
	let ballFire = SKSpriteNode(imageNamed: "fire")
	let ballGhost = SKSpriteNode(imageNamed: "ghost")
	let ballLead = SKSpriteNode(imageNamed: "lead")
	let ballGum = SKSpriteNode(imageNamed: "gum")
	
	//Verifica se o usuário está no menu
	var gameMenu: Bool!
	
	/* criação da tela inicial */
	let mainScreen = SKSpriteNode(imageNamed: "mainScreen")
	
	/* criação buttonNewGame */
	let buttonNewGame = SKSpriteNode(imageNamed: "play")
	
	/* criação Menu Settings */
	let buttonSettings = SKSpriteNode(imageNamed: "settings")
	var buttonSound = SKSpriteNode(imageNamed: "soundOn")
	var buttonVibration = SKSpriteNode(imageNamed: "vibrationOn")
	let buttoniAd = SKSpriteNode(imageNamed: "iAd")
	var buttonSettingsOn = true
	
	/* criação gameCenterButton */
	let buttonGameCenter = SKSpriteNode(imageNamed: "gameCenter")
	
	let actionTap1 = SKAction.scaleTo(1.15, duration: 0.1)
	let actionTap2 = SKAction.scaleTo(0.85, duration: 0.1)
	let actionTap3 = SKAction.scaleTo(1.00, duration: 0.1)
	
	/* criação do som, vibração e iAd*/
	var soundOn : Bool = true
	var vibrationOn : Bool = true
	var audioPlayer : AVAudioPlayer!
	var iAdOn : Bool!
	
	init(size: CGSize, sound: Bool, vibration: Bool, iAd: Bool) {
		soundOn = sound
		vibrationOn = vibration
		iAdOn = iAd
		super.init(size: size)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func didMoveToView(view: SKView) {
		
		gameMenu = true
		
		/* configura screen do menu pausa */
		mainScreen.size = CGSize(width: size.width, height: size.height)
		mainScreen.position = CGPoint(x: size.width/2, y: size.height/2)
		mainScreen.zPosition = -1
		addChild(mainScreen)
		
		/* button newGame */
		buttonNewGame.zPosition = 6
		buttonNewGame.size = CGSize(width: 550, height: 550)
		buttonNewGame.position = CGPoint(x: size.width/2, y: size.height/2) // - 225)
		addChild(buttonNewGame)
		
		/* button settings */
		buttonSettings.zPosition = 5
		buttonSettings.size = CGSize(width: 200, height: 200)
		buttonSettings.position = CGPoint(x: 342, y: 150)
		addChild(buttonSettings)
		
		/* button gameCenter */
		buttonGameCenter.zPosition = 4
		buttonGameCenter.size = CGSize(width: 200, height: 200)
		buttonGameCenter.position = CGPoint(x: 342, y: 434)
		addChild(buttonGameCenter)
		
		
		/******* Background Sound *******/
		let soundFilePath = NSBundle.mainBundle().pathForResource("menuSound", ofType: "mp3")
		let soundFileURL = NSURL.fileURLWithPath(soundFilePath!)
		do {
			try audioPlayer = AVAudioPlayer(contentsOfURL: soundFileURL)
		} catch {
			print("Sound not reached.")
		}
		audioPlayer.numberOfLoops = -1
		audioPlayer.prepareToPlay()
		audioPlayer.volume = 0.0
		audioPlayer.play()
		if soundOn {
			doVolumeFadeIn()
		}
		
	}
	
	override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
		
		let sequenceTap = SKAction.sequence([actionTap1, actionTap2, actionTap3])
		
		for touch: AnyObject in touches {
			let location = touch.locationInNode(self)
			
			if buttonNewGame.containsPoint(location) {
				buttonNewGame.runAction(sequenceTap, completion: {
					self.actionNewGame()
				})
			}
			else if buttonGameCenter.containsPoint(location) {
				buttonGameCenter.runAction(sequenceTap)
				actionGameCenter()
			}
			else if buttonSettings.containsPoint(location) {
				buttonSettings.runAction(sequenceTap)
				actionSettings()
			}
			else if buttonSound.containsPoint(location) {
				if soundOn {
					buttonSound.texture = SKTexture(imageNamed: "soundOff")
					buttonSound.runAction(sequenceTap)
					//Código para desativar o som de background
					doVolumeFadeOut()
					soundOn = false
				}
				else {
					buttonSound.texture = SKTexture(imageNamed: "soundOn")
					buttonSound.runAction(sequenceTap)
					
					//Código para ativar o som de background
					audioPlayer.volume = 0.0
					doVolumeFadeIn()
					soundOn = true
				}
			}
			else if buttonVibration.containsPoint(location) {
				if vibrationOn {
					buttonVibration.texture = SKTexture(imageNamed: "vibrationOff")
					buttonVibration.runAction(sequenceTap)
					
					vibrationOn = false
				}
				else {
					buttonVibration.texture = SKTexture(imageNamed: "vibrationOn")
					buttonVibration.runAction(sequenceTap)
					
					//Vibrar 1 vez
					AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
					
					vibrationOn = true
				}
			}
			else if iAdOn! && buttoniAd.containsPoint(location) {
				buttoniAd.runAction(sequenceTap)
				//Página do app
			}
		}
	}
	
	/* Fade-in/Fade-out da música de fundo */
	func doVolumeFadeOut() {
		if (audioPlayer.volume >= 0.1) {
			audioPlayer.volume = audioPlayer.volume - 0.1
			//delay de meio segundo para o volume chegar no mínimo
			let delay = 0.05 * Double(NSEC_PER_SEC)
			let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
			dispatch_after(time, dispatch_get_main_queue()) {
				self.doVolumeFadeOut()
			}
		}
	}
	func doVolumeFadeIn() {
		if (audioPlayer.volume < 1.0) {
			audioPlayer.volume = audioPlayer.volume + 0.1
			//delay de meio segundo para o volume chegar no máximo
			let delay = 0.05 * Double(NSEC_PER_SEC)
			let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
			dispatch_after(time, dispatch_get_main_queue()) {
				self.doVolumeFadeIn()
			}
		}
	}
	
	/* fazendo transição das cenas */
	func actionNewGame() {
		
		if soundOn {
			//parar a música
			doVolumeFadeOut()
			audioPlayer.volume = 0.0
			audioPlayer.stop()
		}
		
		gameMenu = false
		
		if let controller = self.view?.window?.rootViewController as? GameViewController {
			
			controller.gameScene = GameScene(size: size, sound: soundOn, vibration: vibrationOn, iAd: iAdOn)
			controller.gameScene.scaleMode = scaleMode
			
			//		let reveal = SKTransition.revealWithDirection(SKTransitionDirection.Down, duration: 1.0)
			let reveal = SKTransition.fadeWithColor(UIColor(red: 0.933, green: 0.866, blue: 0.752, alpha: 1.0), duration: 1.0)
			
			view?.presentScene(controller.gameScene, transition: reveal)
		}
	//	}
	}
	
	func actionGameCenter() {
		GameKitHelper.sharedInstance.showGKGameCenterViewController(view?.window?.rootViewController)
	}
	
	func actionSettings() {
		
		if buttonSettingsOn {
			
			if soundOn {
				buttonSound = SKSpriteNode(imageNamed: "soundOn")
			}
			else {
				buttonSound = SKSpriteNode(imageNamed: "soundOff")
			}
			if vibrationOn {
				buttonVibration = SKSpriteNode(imageNamed: "vibrationOn")
			}
			else {
				buttonVibration = SKSpriteNode(imageNamed: "vibrationOff")
			}
			buttonSound.size = CGSize(width: 200, height: 200)
			buttonSound.position = buttonSettings.position
			buttonSound.zPosition = 2
			addChild(buttonSound)
			
			buttonVibration.size = CGSize(width: 200, height: 200)
			buttonVibration.position = buttonSettings.position
			buttonVibration.zPosition = 1
			addChild(buttonVibration)
			
			if iAdOn! {
				buttoniAd.size = CGSize(width: 200, height: 200)
				buttoniAd.position = buttonSettings.position
				buttoniAd.zPosition = 3
				addChild(buttoniAd)
			}
			
			userInteractionEnabled = false
			buttonSound.runAction(SKAction.moveToX(568 + buttonSettings.position.x, duration: 0.2), completion: { () -> Void in
				self.userInteractionEnabled = true
			})
			buttonVibration.runAction(SKAction.moveToX(284 + buttonSettings.position.x, duration: 0.2))
			
			if iAdOn! {
				buttoniAd.runAction(SKAction.moveToX(852 + buttonSettings.position.x, duration: 0.2))
			}
			
			buttonSettingsOn = false
		}
		else {
			userInteractionEnabled = false
			buttonSound.runAction(SKAction.moveToX(buttonSettings.position.x, duration: 0.2), completion: {
				self.userInteractionEnabled = true
				self.buttonSound.removeFromParent()
			})
			
			buttonVibration.runAction(SKAction.moveToX(buttonSettings.position.x, duration: 0.2), completion: {
				self.buttonVibration.removeFromParent()
			})
			
			if iAdOn! {
				buttoniAd.runAction(SKAction.moveToX(buttonSettings.position.x, duration: 0.2), completion: {
					self.buttoniAd.removeFromParent()
				})
			}
			buttonSettingsOn = true
		}
	}
	
}
