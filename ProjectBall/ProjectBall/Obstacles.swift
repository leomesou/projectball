//
//  Obstacles.swift
//  ProjectBall
//
//  Created by Rodrigo Bruno Mendes Theóphilo on 27/03/15.
//  Copyright (c) 2015 LPR. All rights reserved.
//

import SpriteKit

let obstaclesBonus = [
	"water": SKTexture(imageNamed:"water"),
	"fire" : SKTexture(imageNamed:"fire" ),
	"ghost": SKTexture(imageNamed:"ghost"),
	"gum"  : SKTexture(imageNamed:"gum"),
	"lead" : SKTexture(imageNamed:"lead")]

let obstacles = [
	"block": SKTexture(imageNamed:"block"),
	"triangle" : SKTexture(imageNamed:"triangle" ),
	"rectangle": SKTexture(imageNamed:"rectangle"),
	"rectangleFire"  : SKTexture(imageNamed:"rectangleFire"  ),
	"shuriken" : SKTexture(imageNamed:"shuriken" )]

struct PhysicsCategory {
	static let None: UInt32 = 0
	static let Ball: UInt32 = 0b1 // 1
	static let Obstacle: UInt32 = 0b10 // 2
	static let Edge: UInt32 = 0b100 // 4
	static let ObstacleBonus: UInt32 = 0b1000 // 8
}

func getObstaclesBonus() -> Dictionary<String, SKTexture!>{
	return obstaclesBonus
}

func createObstacle(imageName : String) -> SKSpriteNode {
	let obstacleTexture = obstacles[imageName]
	//12,5% a porcentagem da largura da tela para caber 8 obstáculos
	let obstacle = SKSpriteNode(texture:obstacleTexture, color:UIColor.clearColor(), size: CGSize(width: 230.4, height: 230.4))
	
	obstacle.zPosition = 1
	if imageName == "block"{
		obstacle.physicsBody = SKPhysicsBody(rectangleOfSize: obstacle.size)
		
		obstacle.name = "obstacle1"
	}else if imageName == "triangle"{
		obstacle.size = CGSize(width: 154.5, height: 175)
		obstacle.anchorPoint = CGPoint(x: 0.5, y: 0.5)
		let offsetX = obstacle.frame.size.width * obstacle.anchorPoint.x
		let offsetY = obstacle.frame.size.height * obstacle.anchorPoint.y
		
		let path = CGPathCreateMutable();	
		CGPathMoveToPoint(path, nil, 77 - offsetX, 16 - offsetY);
		CGPathAddLineToPoint(path, nil, 53 - offsetX, 41 - offsetY);
		CGPathAddLineToPoint(path, nil, 19 - offsetX, 49 - offsetY);
		CGPathAddLineToPoint(path, nil, 27 - offsetX, 87 - offsetY);
		CGPathAddLineToPoint(path, nil, 18 - offsetX, 123 - offsetY);
		CGPathAddLineToPoint(path, nil, 54 - offsetX, 133 - offsetY);
		CGPathAddLineToPoint(path, nil, 78 - offsetX, 156 - offsetY);
		CGPathAddLineToPoint(path, nil, 101 - offsetX, 133 - offsetY);
		CGPathAddLineToPoint(path, nil, 135 - offsetX, 122 - offsetY);
		CGPathAddLineToPoint(path, nil, 128 - offsetX, 86 - offsetY);
		CGPathAddLineToPoint(path, nil, 132 - offsetX, 51 - offsetY);
		CGPathAddLineToPoint(path, nil, 102 - offsetX, 41 - offsetY);

		CGPathCloseSubpath(path);
		obstacle.physicsBody = SKPhysicsBody(polygonFromPath: path)
		
		obstacle.zRotation = CGFloat(arc4random_uniform(360))
		obstacle.runAction(SKAction.repeatActionForever(SKAction.rotateByAngle(CGFloat(M_PI), duration: 0.25)))
		
		let scaleDown = SKAction.scaleBy(0.2, duration: 2.5)
		let scale = SKAction.sequence([scaleDown, scaleDown.reversedAction()])
		scale.timingMode = SKActionTimingMode.EaseInEaseOut
		obstacle.runAction(SKAction.repeatActionForever(scale), withKey: "scale")
		
		let fadeOut = SKAction.fadeOutWithDuration(2.5)
		let fadeIn = SKAction.fadeInWithDuration(2.5)
		let fade = SKAction.sequence([fadeOut, fadeIn])
		fade.timingMode = SKActionTimingMode.EaseInEaseOut
		let physicsOut = SKAction.runBlock({ () -> Void in
			obstacle.physicsBody?.categoryBitMask = PhysicsCategory.None
			obstacle.zPosition = 0
		})
		let physicsIn = SKAction.runBlock({ () -> Void in
			obstacle.physicsBody?.categoryBitMask = PhysicsCategory.Obstacle
			obstacle.zPosition = 1
		})
		let physicsAction = SKAction.sequence([SKAction.waitForDuration(0.5), physicsOut, SKAction.waitForDuration(4), physicsIn, SKAction.waitForDuration(0.5)])
		obstacle.runAction(SKAction.repeatActionForever(SKAction.group([fade, physicsAction])), withKey: "fade")
		
		obstacle.name = "obstacle2"
	}else if imageName == "rectangle" || imageName == "rectangleFire"{
		obstacle.size = CGSize(width: 230, height: 15)
		
		let offsetX = obstacle.frame.size.width * obstacle.anchorPoint.x
		let offsetY = obstacle.frame.size.height * obstacle.anchorPoint.y
		
		let path = CGPathCreateMutable();
		CGPathMoveToPoint(path, nil, 0 - offsetX, 0 - offsetY);
		CGPathAddLineToPoint(path, nil, 0 - offsetX, 15 - offsetY);
		CGPathAddLineToPoint(path, nil, 230 - offsetX, 15 - offsetY);
		CGPathAddLineToPoint(path, nil, 230 - offsetX, 0 - offsetY);
		CGPathCloseSubpath(path);
		
		obstacle.physicsBody = SKPhysicsBody(polygonFromPath: path)
		
		//retângulo de fogo
		if imageName == "rectangleFire"{
			let fire = createBurningBox(obstacle)
			let rotation = SKAction.repeatActionForever(SKAction.rotateByAngle(CGFloat(M_PI), duration: 3))
			let moveToLeft = SKAction.moveByX(-obstacle.size.width/2, y: 0, duration: 1.5)
			let moveLeft = SKAction.sequence([moveToLeft, moveToLeft.reversedAction()])
			let moveToRight = SKAction.moveByX(obstacle.size.width/2, y: 0, duration: 1.5)
			let moveRight = SKAction.sequence([moveToRight, moveToRight.reversedAction()])
			var move = SKAction.repeatActionForever(SKAction.sequence([moveLeft, moveRight]))
			if arc4random_uniform(2) == 1{
				move = move.reversedAction()
			}
			fire.position = CGPoint(x: 0, y: 0)
			fire.numParticlesToEmit = 0
			obstacle.addChild(fire)
			obstacle.runAction(rotation)
			obstacle.runAction(SKAction.sequence([move]))
			obstacle.alpha = 0.6
			obstacle.name = "obstacle4"
		}else{
			obstacle.name = "obstacle3"
		}
	}

	obstacle.physicsBody!.dynamic = false
	obstacle.physicsBody!.allowsRotation = false
	obstacle.physicsBody!.restitution = 0.0 //Energia que o corpo mantém depois da colisão
	obstacle.physicsBody!.linearDamping = 1 //Perda de velocidade
	
	obstacle.physicsBody!.categoryBitMask = PhysicsCategory.Obstacle
	obstacle.physicsBody!.collisionBitMask = PhysicsCategory.Ball
	obstacle.physicsBody!.contactTestBitMask = PhysicsCategory.Ball
	
	return obstacle
}

func randomObstacleName(stage : Int) -> String{
	let obstacle : String
	let random = arc4random_uniform(100) + 1
	if stage == 1{
		if random <= 50{
			obstacle = "rectangle"
		}else{
			obstacle = "block"
		}
	} else if stage == 2{
		if random <= 20{
			obstacle = "rectangleFire"
		}else if random <= 60{
			obstacle = "rectangle"
		}else{
			obstacle = "block"
		}
	} else if stage == 3{
		if random <= 20{
			obstacle = "triangle"
		}else if random <= 60{
			obstacle = "rectangle"
		}else{
			obstacle = "block"
		}
	} else {
		if random <= 10{
			obstacle = "triangle"
		}else if random <= 20{
			obstacle = "rectangleFire"
		}else if random <= 60{
			obstacle = "rectangle"
		}else{
			obstacle = "block"
		}
	}
	return obstacle
}
func bonusForObstacle(obstacle : SKNode) -> Int{
	let bonus : Int
	if obstacle.name == "obstacle1"{
		bonus = 10
	}
	else if obstacle.name == "obstacle2"{
		bonus = 60
	}
	else if obstacle.name == "obstacle3"{
		bonus = 20
	}
	else if obstacle.name == "obstacle4"{
		bonus = 40
	}
	else{
		bonus = 40
	}
	return bonus
}
func generateRandomObstacle(inout possibleRegions : [Double], heightToSpawn: CGFloat, stage : Int) -> SKSpriteNode{
	var heightToSpawn = heightToSpawn
	let obstacleToBeSpawned = randomObstacleName(stage)
	if obstacleToBeSpawned == "rectangle" || obstacleToBeSpawned == "rectangleFire"{
		heightToSpawn = heightToSpawn + CGFloat(100)
	}else if obstacleToBeSpawned == "triangle"{
		heightToSpawn = heightToSpawn + CGFloat(25)
	}
	//Se tiver retângulos de fogo a linha é fechada, no máximo uma vez a cada duas linnhas
	let randomIndex = Int(arc4random_uniform(UInt32(possibleRegions.count)))
	let positionOfThisNode = possibleRegions[randomIndex]
	possibleRegions.removeAtIndex(randomIndex)
	let newObstacle = createObstacle(obstacleToBeSpawned)
	newObstacle.position = CGPoint(x: CGFloat(positionOfThisNode), y: heightToSpawn + newObstacle.size.height/2)
	return newObstacle
}

func createBonus(sizeOfScreen : CGSize) -> SKSpriteNode {
	//Endereço random dicionário de obstáculos bonus
	let randomIndex = Int(arc4random_uniform(UInt32(obstaclesBonus.count)))
	//Textura referente à essa posição random no dicionário
	let texture = Array(obstaclesBonus.values)[randomIndex]
	let bonus = SKSpriteNode(texture: texture, color:UIColor.clearColor(), size:CGSize(width: sizeOfScreen.width*0.08, height: sizeOfScreen.width*0.08))
	bonus.name = "bonus"
	
	bonus.zPosition = 2
	
	bonus.physicsBody = SKPhysicsBody(circleOfRadius: bonus.size.width/2, center: bonus.position)
	bonus.physicsBody!.dynamic = false
	bonus.physicsBody!.allowsRotation = false
	
	bonus.physicsBody!.categoryBitMask = PhysicsCategory.ObstacleBonus
	bonus.physicsBody!.collisionBitMask = PhysicsCategory.Ball
	bonus.physicsBody!.contactTestBitMask = PhysicsCategory.Ball
	
	return bonus
	
}
func sendTriangleToPatrol(triangle : SKSpriteNode, freeSpot : CGPoint){
	triangle.removeActionForKey("scale")
	triangle.removeActionForKey("fade")
	triangle.texture = SKTexture(imageNamed: "shuriken.png")
	triangle.name = "obstacle5"
	triangle.xScale = 0.75; triangle.yScale = 0.75
	let moveUp = SKAction.moveByX(0, y: 200, duration: 0.4)
	let moveSide = SKAction.moveByX(freeSpot.x - triangle.position.x, y: 0, duration: 2)
	let moveDown = SKAction.moveByX(0, y: -400, duration: 0.8)
	let moveBack = moveSide.reversedAction()
	let move = SKAction.sequence([moveUp, moveSide, moveDown, moveBack, moveUp])
	triangle.runAction(SKAction.repeatActionForever(move))
}
func attachObstacleToBall(obstacle obstacle : SKSpriteNode, ball: Ball, contactPoint: CGPoint) -> SKPhysicsJointPin{
	obstacle.removeAllActions()
	obstacle.removeFromParent()
	obstacle.physicsBody?.dynamic = true
	let attachment = SKPhysicsJointPin.jointWithBodyA(ball.physicsBody!, bodyB: obstacle.physicsBody!, anchor: contactPoint)
	ball.addChild(obstacle)
	obstacle.zPosition = -1
	return attachment
}
func obstacleFlashing(obstacle : SKSpriteNode){
	let flashOut = SKAction.runBlock { () -> Void in
		obstacle.alpha = 0.2
	}
	let flashIn = SKAction.runBlock { () -> Void in
		obstacle.alpha = 1.0
	}
	let wait1 = SKAction.waitForDuration(0.25)
	let wait2 = SKAction.waitForDuration(0.1)
	let flash = SKAction.sequence([SKAction.waitForDuration(1), flashOut, wait1, flashIn, wait1, flashOut, wait1, flashIn, wait1, flashOut, wait2, flashIn, wait2, flashOut, wait2, flashIn, wait2, flashOut, wait2, flashIn, wait2, flashOut, wait2, flashIn, wait2, flashOut, wait2, flashIn])
	obstacle.runAction(flash)
}
func vanishObstacle(obstacle: SKSpriteNode) -> SKCropNode{
	let masked = SKSpriteNode(color: UIColor.clearColor(), size: obstacle.frame.size)
	let shown = SKSpriteNode(color: UIColor.whiteColor(), size: obstacle.frame.size)
	//
	let maskNode = SKNode()
	maskNode.addChild(masked)
	maskNode.addChild(shown)
	let cropNode = SKCropNode()
	cropNode.maskNode = maskNode
	cropNode.position = obstacle.position
	obstacle.removeFromParent()
	
	cropNode.addChild(obstacle)
	obstacle.physicsBody?.categoryBitMask = PhysicsCategory.None
	let vanishUp = SKAction.moveBy(CGVector(dx: 0, dy: obstacle.frame.size.height), duration: 0.5)
	let remove = SKAction.runBlock { () -> Void in
		cropNode.removeAllChildren()
		cropNode.removeFromParent()
	}
	maskNode.runAction(SKAction.sequence([vanishUp, remove]))
	return cropNode
}
func pushObstacle(obstacle: SKSpriteNode, contact: SKPhysicsContact){
	obstacle.removeAllActions()
	obstacle.zPosition = 10
	obstacle.physicsBody?.dynamic = true
	obstacle.physicsBody?.allowsRotation = true
	let impulse = CGVector(dx: contact.contactNormal.dx, dy: -contact.contactNormal.dy)
	obstacle.physicsBody?.applyImpulse(impulse, atPoint: contact.contactPoint)
	
	obstacle.physicsBody?.categoryBitMask = PhysicsCategory.None
	let scale = SKAction.scaleBy(2, duration: 0.5)
	let fade = SKAction.fadeOutWithDuration(0.5)
	let scaleAndFade = SKAction.group([scale, fade])
	let remove = SKAction.removeFromParent()
	obstacle.runAction(SKAction.sequence([scaleAndFade, remove]))
}


//PARA DAR SENTIDO AO FUNDO MATAR A BOLA
//func createAbstractObstacles

