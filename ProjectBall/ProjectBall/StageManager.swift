//
//  StageManager.swift
//  ProjectBall
//
//  Created by Rodrigo Bruno Mendes Theóphilo on 11/07/15.
//  Copyright (c) 2015 LPR. All rights reserved.
//

import SpriteKit

let colorTransitions = [
	"color1To2": SKTexture(imageNamed:"color1To2"),
	"color2To3" : SKTexture(imageNamed:"color2To3"),
	"color3To4" : SKTexture(imageNamed:"color3To4")]

func startStage(stage: Int, scene : GameScene){
	//Evita que chame mais obstáculos durante a transição
	scene.pointsToSpawnObstacles = scene.distanceToSpawnObstacles * 100
	if stage == 2{
		background = "backgroundGame2"
	}else if stage == 3{
		background = "backgroundGame3"
	}else if stage == 4{
		background = "backgroundGame4"
	}
	//tempo sem obstáculos e antes da parede
	let wait1 = SKAction.waitForDuration(2.0)
	let transition = SKAction.runBlock { () -> Void in
		if !scene.gameOverActive{
			createTransitionWall(scene, stage: stage)
		}
	}
	let duration = (scene.size.height/scene.backgroundMovePointsPerSec) + (900/scene.backgroundMovePointsPerSec)
	//tempo que a parede desce
	let wait2 = SKAction.waitForDuration(Double(duration))
	let changeBackground = SKAction.runBlock { () -> Void in
		if !scene.gameOverActive{
		//Muda pro novo background
		scene.updateBackground()
		//para de andar o jogo para não matar a bola
		scene.backgroundMovePointsPerSec = 0.0
		}
	}
	scene.runAction(SKAction.sequence([wait1, transition, wait2, changeBackground]))
}

func createTransitionWall(scene : GameScene, stage : Int)  {
	//Cor de transição
	let transitionOfStages : String
	if stage == 2{
		transitionOfStages = "color1To2"
	}else if stage == 3{
		transitionOfStages = "color2To3"
	}else{
		transitionOfStages = "color3To4"
	}
	//Coloca mais uma camada de background com a cor nova debaixo do wall
	let backgroundTemp = SKSpriteNode(texture: backgrounds[background])
	backgroundTemp.zPosition = 0
	backgroundTemp.anchorPoint = CGPointZero
	backgroundTemp.position = CGPoint(x: 0, y: scene.size.height + 900)
	scene.addChild(backgroundTemp)
	
	/* Sobe o paredão e o adiciona à tela */
	scene.wall.position = CGPoint(x: 0, y: scene.size.height + 600)
	scene.addChild(scene.wall)
	/* Label do paredão */
	let stageLabel = NSLocalizedString("Stage", comment: "Stage")
	scene.lbStage.text = "\(stageLabel) \(stage)"
	scene.wall.addChild(scene.lbStage)
	/* Pequena transição entre as cores */
	scene.colorTransition.texture = colorTransitions[transitionOfStages]
	scene.addChild(scene.colorTransition)
	
	//Altura da tela(2048)/Velocidade do background + 600/Velocidade do background
	let duration = (scene.size.height/scene.backgroundMovePointsPerSec) + (900/scene.backgroundMovePointsPerSec)
	let move = SKAction.moveByX(0, y: -scene.size.height - 900, duration: Double(duration))
	//1 segundo parado
	let wait = SKAction.group([SKAction.waitForDuration(1.0), SKAction.runBlock({ () -> Void in
		//Ganha bonus de score por completar a fase!
		let (lbScoreBonus, bonusGained) = scoreBonus(stage)
		scene.scoreValue += bonusGained
		scene.score.text = String(scene.scoreValue)
		scene.addChild(lbScoreBonus)
	})])
	//Metade da duração pra descer a tela
	let moveMore = SKAction.moveByX(0, y: -scene.size.height, duration: Double(duration/3))
	
	scene.colorTransition.runAction(SKAction.sequence([move, SKAction.waitForDuration(1.0), moveMore]), completion: { () -> Void in
		scene.colorTransition.removeFromParent()
	})
	scene.wall.runAction(SKAction.sequence([move, wait, moveMore]), completion: { () -> Void in
		scene.wall.removeAllChildren()
		scene.wall.removeFromParent()
		//Reinicia a velocidade do jogo
		scene.backgroundMovePointsPerSec = 200.0 + CGFloat(stage*100)/4
		//Aumenta o velocidade máxima pro próximo nível
		scene.maxVelocityOfStage += 100.0
		//Diminue o tempo pro próximo spawn
		scene.pointsToSpawnObstacles = scene.distanceToSpawnObstacles/10
	})
	backgroundTemp.runAction(SKAction.sequence([move, SKAction.waitForDuration(1.0)]), completion: { () -> Void in
		backgroundTemp.removeFromParent()
	})
}
func scoreBonus(stage : Int) -> (SKLabelNode, Int){
	let lbScoreBonus = SKLabelNode(fontNamed: fontName)
	lbScoreBonus.fontColor = SKColor.whiteColor()
	let bonusGained = (stage-1) * 250
	lbScoreBonus.text = "+\(bonusGained)"
	lbScoreBonus.fontSize = 100
	lbScoreBonus.position = CGPoint(x: 900, y: 1885)
	lbScoreBonus.zPosition = 30
	lbScoreBonus.xScale = 0.5; lbScoreBonus.yScale = 0.5
	let scale = SKAction.scaleBy(3, duration: 0.5)
	let fade = SKAction.fadeOutWithDuration(2.0)
	lbScoreBonus.runAction(SKAction.sequence([scale, fade]), completion: { () -> Void in
		lbScoreBonus.removeFromParent()
	})
	return (lbScoreBonus, bonusGained)
}
func scoreObstacle(bonusGained : Int, position : CGPoint, scene : GameScene){
	
	scene.scoreValue += bonusGained
	
	let lbScoreObstacle = SKLabelNode(fontNamed: fontName)
	lbScoreObstacle.fontSize = 30
	lbScoreObstacle.zPosition = 30
	lbScoreObstacle.text = "+\(bonusGained)"
	lbScoreObstacle.position = position
	let scale = SKAction.scaleBy(3, duration: 0.5)
	let fade = SKAction.fadeOutWithDuration(1.0)
	scene.addChild(lbScoreObstacle)
	lbScoreObstacle.runAction(SKAction.sequence([scale, fade]), completion: { () -> Void in
		lbScoreObstacle.removeFromParent()
	})
}