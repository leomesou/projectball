//
//  GameViewController.swift
//  ProjectBall
//
//  Created by Leandro on 12/03/15.
//  Copyright (c) 2015 LPR. All rights reserved.
//

import UIKit
import SpriteKit
import iAd

class GameViewController: UIViewController, ADBannerViewDelegate{
	
	var gameScene : GameSceneInterface!
	
	//Criação do iAd
	var iAdOn: Bool = false
	var bannerView: ADBannerView?

	override func viewDidLoad() {
		
		//GameCenter
		NSNotificationCenter.defaultCenter().addObserver(self, selector:
			Selector("showAuthenticationViewController"), name:
			PresentAuthenticationViewController, object: nil)
		GameKitHelper.sharedInstance.authenticateLocalPlayer()
		
		super.viewDidLoad()
		
		gameScene = MainMenu(size: CGSize(width: 1536, height: 2048), sound: true, vibration: true, iAd: iAdOn)
		
		let skView = self.view as! SKView
		//skView.showsFPS = true
		//skView.showsNodeCount = true
		//skView.showsPhysics = true
		//Diminue o limit de frames pela metade
		//skView.frameInterval = 2
		/* Sprite Kit applies additional optimizations to improve rendering performance */
		skView.ignoresSiblingOrder = true
		
		/* Set the scale mode to scale to fit the window */
		gameScene.scaleMode = .AspectFill
		gameScene.backgroundColor = UIColor.whiteColor()
		skView.presentScene(gameScene)
		
		//iAd
		
		if iAdOn {
			self.canDisplayBannerAds = true
			self.bannerView?.delegate = self
		}
	}
	
	override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
		if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
			return UIInterfaceOrientationMask.AllButUpsideDown
		} else {
			return UIInterfaceOrientationMask.All
		}
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Release any cached data, images, etc that aren't in use.
	}
	
	override func prefersStatusBarHidden() -> Bool {
		return true
	}
	
	override func shouldAutorotate() -> Bool {
		return true
	}
	
	//iAds
	func bannerViewWillLoadAd(banner: ADBannerView!) {
		
	}
	func bannerViewDidLoadAd(banner: ADBannerView!) {
		self.bannerView?.hidden = true
	}
	func bannerViewActionDidFinish(banner: ADBannerView!) {
		
	}
	func bannerViewActionShouldBegin(banner: ADBannerView!, willLeaveApplication willLeave: Bool) -> Bool {
		return true
	}
	func bannerView(banner: ADBannerView!, didFailToReceiveAdWithError error: NSError!) {
	}
	
	//GameCenter
	func showAuthenticationViewController() {
		
		let gameKitHelper = GameKitHelper.sharedInstance
		
		if let authenticationViewController = gameKitHelper.authenticationViewController {
			presentViewController(authenticationViewController, animated: true, completion: nil)
		}
	}
	deinit {
		NSNotificationCenter.defaultCenter().removeObserver(self)
	}
}
