//
//  Effects.swift
//  ProjectBall
//
//  Created by Rodrigo Bruno Mendes Theóphilo on 27/03/15.
//  Copyright (c) 2015 LPR. All rights reserved.
//

import SpriteKit

func createTrail(ballType : String) -> SKEmitterNode{
	var engineEmitter : SKEmitterNode = SKEmitterNode()
	switch ballType{
	case "ballWater":
//		engineEmitter = SKEmitterNode(fileNamed: "SnowTrail.sks")!
		engineEmitter = SKEmitterNode(fileNamed: "BubbleTrail.sks")!
		engineEmitter.physicsBody?.dynamic = false
	case "ballFire":
		engineEmitter = SKEmitterNode(fileNamed: "FireTrail.sks")!
	case "ballGhost":
		engineEmitter = SKEmitterNode(fileNamed: "GhostTrail.sks")!
	default:
		engineEmitter.particleColor = SKColor.grayColor()
	}
    engineEmitter.position = CGPointMake(0, 0)
    engineEmitter.name = "particle"
    engineEmitter.zPosition = 20
	
    return engineEmitter
}

func createExplosionBall(ballType : String, ballSprite: Ball) -> SKEmitterNode{
	
//	let engineEmitter = SKEmitterNode(fileNamed: "ExplosionBall.sks")!
	let engineEmitter = SKEmitterNode(fileNamed: "BallDeath.sks")!
	engineEmitter.position = ballSprite.position + CGPoint(x: 0, y: 75)
	engineEmitter.name = "particle"
	engineEmitter.zPosition = 3
	engineEmitter.particleColorSequence = nil
	engineEmitter.particleColorBlendFactor = 1
	switch ballType{
	case "ballWater":
		engineEmitter.particleColor = SKColor.cyanColor()
	case "ballFire":
		engineEmitter.particleColor = SKColor.orangeColor()
	case "ballGhost":
		engineEmitter.particleColor = SKColor.whiteColor()
	case "ballGum":
		engineEmitter.particleColor = SKColor.magentaColor()
	case "ballLead":
		engineEmitter.particleColor = SKColor.blackColor()
	default:
		engineEmitter.particleColor = SKColor.grayColor()
	}
	//engineEmitter.particleColorSequence = nil
	return engineEmitter
}

func createExplosionBox(node : SKNode) -> SKEmitterNode{
	
	let engineEmitter : SKEmitterNode
	if node.name == "obstacle2" || node.name == "obstacle5" || node.name == "obstacle3"{
		engineEmitter = SKEmitterNode(fileNamed: "obstacleExplosion.sks")!
		if node.name == "obstacle3"{
			engineEmitter.particlePositionRange = CGVector(dx: 200, dy: 20)
		}else{
			engineEmitter.particlePositionRange = CGVector(dx: 80, dy: 80)
		}
		engineEmitter.position = node.position + CGPoint(x: 0, y: 0)
	}else{
//		engineEmitter = SKEmitterNode(fileNamed: "ExplosionBox.sks")!
		engineEmitter = SKEmitterNode(fileNamed: "obstacleExplosion.sks")!
	}
	engineEmitter.position = node.position
	engineEmitter.name = "particle"
	engineEmitter.zPosition = 30
	return engineEmitter
}

func createGigaExplosion(node : SKNode, ballType : String) -> (SKEmitterNode, SKEmitterNode){
	var fileName1 = ""
	var fileName2 = ""
	if ballType == "ballWater"{
		fileName1 = "WaveExplosionLeft.sks"
		fileName2 = "WaveExplosionRight.sks"
	}
	else if ballType == "ballFire"{
		fileName1 = "fireExplosionTrail.sks"
		fileName2 = "fireExplosion.sks"
	}else{
		fileName1 = "fireExplosionTrail.sks"
		fileName2 = "GhostExplosion.sks"
	}
	let engineEmitter1 = SKEmitterNode(fileNamed: fileName1)!
	engineEmitter1.position = node.position
	engineEmitter1.name = "particle"
	engineEmitter1.zPosition = 40

	let engineEmitter2 = SKEmitterNode(fileNamed: fileName2)!
	engineEmitter2.position = node.position
	engineEmitter2.name = "particle"
	engineEmitter2.zPosition = 40
	
	if ballType == "ballGhost"{
		engineEmitter1.particleColorSequence = nil
		engineEmitter1.particleColorBlendFactor = 1
		engineEmitter1.particleColor = SKColor.whiteColor()
	}
	return (engineEmitter1, engineEmitter2)
}
func createSmoke() -> SKEmitterNode{
	let engineEmitter = SKEmitterNode(fileNamed: "BallSmoking.sks")!
	engineEmitter.position = CGPointMake(0, 0)
	engineEmitter.name = "particleSmoke"
	engineEmitter.zPosition = 30
	return engineEmitter
}
func createSmokeObstacle() -> SKEmitterNode{
	let engineEmitter = SKEmitterNode(fileNamed: "ObstacleSmoking.sks")!
	engineEmitter.position = CGPointMake(0, 0)
	engineEmitter.name = "particleSmoke"
	engineEmitter.zPosition = 30
	return engineEmitter
}
func createConcentratingAura() -> SKEmitterNode{
	let engineEmitter = SKEmitterNode(fileNamed: "Concentrating.sks")!
	engineEmitter.position = CGPointMake(0, 0)
	engineEmitter.name = "particle"
	engineEmitter.zPosition = 40
	return engineEmitter
}
func createCompletedCharge(ballType : String) -> SKEmitterNode{
	let engineEmitter = SKEmitterNode(fileNamed: "ConcentratingComplete.sks")!
	engineEmitter.position = CGPointMake(0, 90)
	engineEmitter.name = "particle"
	engineEmitter.zPosition = 40
	if ballType != "ballDefault"{
		engineEmitter.particleColorSequence = nil
		engineEmitter.particleColorBlendFactor = 1
		if ballType == "ballFire"{
			engineEmitter.particleColor = SKColor(red: 1, green: 128/255.0, blue: 0, alpha: 1)
		}else if ballType == "ballGhost"{
			engineEmitter.particleColor = SKColor(red: 0, green: 128/255.0, blue: 0, alpha: 0.5)
		}else{
			engineEmitter.particleColor = SKColor(red: 0, green: 128/255.0, blue: 1, alpha: 0.5)
		}
	}
	return engineEmitter
}
func createChargeAura() -> SKEmitterNode{
	let engineEmitter = SKEmitterNode(fileNamed: "Charging.sks")!
	engineEmitter.position = CGPointMake(0, 0)
	engineEmitter.name = "particle"
	engineEmitter.zPosition = 20
	return engineEmitter
}

func createBurningBox(node: SKNode) -> SKEmitterNode{
	let engineEmitter: SKEmitterNode
	if node.name == "obstacle2" || node.name == "obstacle5"{
		engineEmitter = SKEmitterNode(fileNamed: "obstacleExplosion.sks")!
		engineEmitter.particlePositionRange = CGVector(dx: 80, dy: 80)
		engineEmitter.position = node.position + CGPoint(x: 0, y: 0)
	}else{
		engineEmitter = SKEmitterNode(fileNamed: "ObstacleBurning.sks")!
		if node.name == "obstacle1"{
			engineEmitter.position = node.position + CGPoint(x: 0, y: -90)
		}else{
			engineEmitter.position = node.position + CGPoint(x: 0, y: 0)
		}
	}
	engineEmitter.name = "particle"
	engineEmitter.zPosition = 40
	return engineEmitter
}